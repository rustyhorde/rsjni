//
//  TestExt Class
//
import java.lang.System;

public class TestExt extends Test {
    public TestExt(int current) {
		super(current);
    }

    public void incrementCurrent() {
		this.current += 2;
	}
}