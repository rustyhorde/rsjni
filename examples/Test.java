
//
//  Test Class
//
import java.lang.System;
import java.util.Arrays;
import java.util.stream.Stream;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

public class Test {
	public int current = 0;
	public static String message = "Hello!";

	public Test(int current) {
		super();
		this.current = current;
	}

	public void incrementCurrent() {
		this.current += 1;
	}

	public int getCurrent() {
		return this.current;
	}

	public static int add(int a, int b) {
		return a + b;
	}

	public static String append(String input) {
		return input + " there from Java!";
	}

	public static void printMessage() {
		System.out.println("The message is: " + message);
	}

	public boolean all(boolean barr[]) {
		final Stream<Boolean> stream = IntStream.range(0, barr.length).mapToObj(idx -> barr[idx]);
		return stream.allMatch(i -> i);
	}

	public boolean[] booleanArr() {
		boolean[] a = {true, false, true, true, false};
		return a;
	}

	public boolean allOnes(byte barr[]) {
		final Stream<Byte> stream = IntStream.range(0, barr.length).mapToObj(idx -> barr[idx]);
		return stream.allMatch(i -> i == 1);
	}

	public byte[] byteArr() {
		byte[] a = {1,2,3,4,5,9};
		return a;
	}

	public boolean allAs(char carr[]) {
		final Stream<Character> stream = IntStream.range(0, carr.length).mapToObj(idx -> carr[idx]);
		return stream.allMatch(i -> i == 'a');
	}

	public char[] charArr() {
		char[] a = {'a','b','c','d','e'};
		return a;
	}

	public boolean allOnes(short sarr[]) {
		final Stream<Short> stream = IntStream.range(0, sarr.length).mapToObj(idx -> sarr[idx]);
		return stream.allMatch(i -> i == 1);
	}

	public short[] shortArr() {
		short[] a = {1,2,3,4,5,9};
		return a;
	}

	public boolean allOnes(int iarr[]) {
		final IntStream stream = Arrays.stream(iarr);
		return stream.allMatch(i -> i == 1);
	}

	public int[] intArr() {
		int[] a = {1,2,3,4,5,9};
		return a;
	}

	public boolean allOnes(long larr[]) {
		final LongStream stream = Arrays.stream(larr);
		return stream.allMatch(i -> i == 1);
	}

	public long[] longArr() {
		long[] a = {1,2,3,4,5,9};
		return a;
	}

	public boolean allOnePointFive(float farr[]) {
		final Stream<Float> stream = IntStream.range(0, farr.length).mapToObj(idx -> farr[idx]);
		return stream.allMatch(i -> i == 1.5f);
	}

	public float[] floatArr() {
		float[] a = {1.1f,2.2f,3.3f,4.4f,5.5f,9.9f};
		return a;
	}

	public boolean allOnePointFive(double darr[]) {
		final DoubleStream stream = Arrays.stream(darr);
		return stream.allMatch(i -> i == 1.5);
	}

	public double[] doubleArr() {
		double[] a = {1.1,2.2,3.3,4.4,5.5,9.9};
		return a;
	}

	public boolean allHi(String sarr[]) {
		final Stream<String> stream = IntStream.range(0, sarr.length).mapToObj(idx -> sarr[idx]);
		return stream.allMatch(i -> i == "Hi");
	}

	public String[] stringArr() {
		String[] a = {"one","two","three","four","five"};
		return a;
	}
}
