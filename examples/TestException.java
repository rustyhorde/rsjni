
//
//  Test Exception Class
//
import java.lang.Exception;

public class TestException extends Exception {
    private static final long serialVersionUID = -6631291006030705175L;

    TestException(String msg) {
        super(msg);
    }
}
