// Copyright (c) 2017 rsjni developers
//
// Licensed under the Apache License, Version 2.0
// <LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0> or the MIT
// license <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. All files in the project carrying such notice may not be copied,
// modified, or distributed except according to those terms.

//! rsjni errors
use ffi::constants::{JNI_EDETACHED, JNI_EEXIST, JNI_EINVAL, JNI_ENOMEM, JNI_EVERSION};

error_chain! {
    foreign_links {
        CharTryFrom(::core::char::CharTryFromError);
        Io(::std::io::Error);
        NulError(::std::ffi::NulError);
        TryFromInt(::core::num::TryFromIntError);
        Utf8(::core::str::Utf8Error);
    }

    errors {
        CanonicalName(name: String) {
            description("Unrecognized canonical name")
            display("Unrecognized canonical name: '{}'", name)
        }
        ClassNotFound(name: String) {
            description("Class not found")
            display("Class '{}' not found", name)
        }
        CreateJVM {
            description("Unable to create the JVM!")
            display("Unable to create the JVM!")
        }
        Detached {
            description("The current thread is not attached to the JVM!")
            display("The current thread is not attached to the JVM!")
        }
        Exists {
            description("The JVM has already been created!")
            display("The JVM has already been created!")
        }
        InvalidArguments {
            description("Invalid arguments have been supplied to the FFI call.")
            display("Invalid arguments have been supplied to the FFI call.")
        }
        InvalidField {
            description("")
            display("")
        }
        InvalidMethod {
            description("")
            display("")
        }
        JNIError(status: i32) {
            description("A JNI error has occurred")
            display("A JNI error '{}' has occurred", status)
        }
        NotAssignableFromThrowable {
            description("The current object is not assignable from 'java/lang/Throwable'")
            display("The current object is not assignable from 'java/lang/Throwable'")
        }
        NotAnObject {
            description("")
            display("")
        }
        NotImplemented {
            description("")
            display("")
        }
        NullPointer {
            description("The FFI call generated a null pointer!")
            display("The FFI call generated a null pointer!")
        }
        OutOfMemory {
            description("The JVM has run out of memory!")
            display("The JVM has run out of memory!")
        }
        TryFromI32(given: i32) {
            description("Unable to convert from the given i32 to a Version")
            display("Unable to convert from the given '{}' to a Version", given)
        }
        Unknown(msg: String) {
            description("An unknown error has occurred!")
            display("An unknown error has occurred {}!", msg)
        }
        Version {
            description("The requested JNI version is not supported!")
            display("The requested JNI version is not supported!")
        }
        VoidKindForField {
            description("void is not a valid field type.")
            display("void is not a valid field type.")
        }
        VoidType {
            description("Cannot convert a void type into an argument type.")
            display("Cannot convert a void type into an argument type.")
        }
    }
}

impl From<i32> for Error {
    fn from(status: i32) -> Self {
        if status == JNI_EDETACHED {
            ErrorKind::Detached.into()
        } else if status == JNI_EVERSION {
            ErrorKind::Version.into()
        } else if status == JNI_ENOMEM {
            ErrorKind::OutOfMemory.into()
        } else if status == JNI_EEXIST {
            ErrorKind::Exists.into()
        } else if status == JNI_EINVAL {
            ErrorKind::InvalidArguments.into()
        } else {
            ErrorKind::JNIError(status).into()
        }
    }
}
