// Copyright (c) 2017 rsjni developers
//
// Licensed under the Apache License, Version 2.0
// <LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0> or the MIT
// license <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. All files in the project carrying such notice may not be copied,
// modified, or distributed except according to those terms.

//! Load platform specific bindings

#[cfg(all(feature = "ten", target_arch = "x86_64", target_os = "windows", target_env = "msvc"))]
mod x86_64_pc_windows_msvc;
#[cfg(all(feature = "ten", target_arch = "x86_64", target_os = "linux"))]
mod x86_64_unknown_linux_gnu;

#[cfg(all(feature = "ten", target_arch = "x86_64", target_os = "windows", target_env = "msvc"))]
pub use self::x86_64_pc_windows_msvc::jni;
#[cfg(all(feature = "ten", target_arch = "x86_64", target_os = "linux"))]
pub use self::x86_64_unknown_linux_gnu::jni;
