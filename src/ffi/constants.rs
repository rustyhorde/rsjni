// Copyright (c) 2017 rsjni developers
//
// Licensed under the Apache License, Version 2.0
// <LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0> or the MIT
// license <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. All files in the project carrying such notice may not be copied,
// modified, or distributed except according to those terms.

//! Java FFI Constants
use ffi::jni::JNINativeInterface_;
use ffi::opaque::{_JavaFieldId, _JavaMethodId, _JavaObject};
use ffi::structs::JNIInvokeInterface_;

/// JNI False
#[allow(dead_code)]
pub const JNI_FALSE: ::std::os::raw::c_uint = 0;
/// JNI True
pub const JNI_TRUE: ::std::os::raw::c_uint = 1;

/// JNI OK
pub const JNI_OK: ::std::os::raw::c_uint = 0;
/// JNI Error
#[allow(dead_code)]
pub const JNI_ERR: ::std::os::raw::c_int = -1;
/// JNI Error `EDETACHED`
pub const JNI_EDETACHED: ::std::os::raw::c_int = -2;
/// JNI Error `EVERSION`
pub const JNI_EVERSION: ::std::os::raw::c_int = -3;
/// JNI Error `ENOMEM`
pub const JNI_ENOMEM: ::std::os::raw::c_int = -4;
/// JNI Error `EEXIST`
pub const JNI_EEXIST: ::std::os::raw::c_int = -5;
/// JNI Error `EINVAL`
pub const JNI_EINVAL: ::std::os::raw::c_int = -6;

/// JNI Commit
#[allow(dead_code)]
pub const JNI_COMMIT: ::std::os::raw::c_uint = 1;
/// JNI Abort
#[allow(dead_code)]
pub const JNI_ABORT: ::std::os::raw::c_uint = 2;

/// The Java Virtual Machine.
pub type JavaVM = *const JNIInvokeInterface_;

/// A Java Integer type mapping
#[cfg(unix)]
pub type JavaInteger = ::std::os::raw::c_int;
/// A Java Integer type mapping
#[cfg(windows)]
pub type JavaInteger = ::std::os::raw::c_long;
/// A Java Long type mapping
#[cfg(all(unix, target_pointer_width = "64"))]
pub type JavaLong = ::std::os::raw::c_long;
/// A Java Long type mapping
#[cfg(any(windows, target_pointer_width = "32"))]
pub type JavaLong = ::std::os::raw::c_longlong;
/// A Java Byte type mapping
pub type JavaByte = ::std::os::raw::c_schar;
/// A Java Boolean type mapping
pub type JavaBoolean = ::std::os::raw::c_uchar;
/// A Java Char type mapping
pub type JavaChar = ::std::os::raw::c_ushort;
/// A Java Short type mapping
pub type JavaShort = ::std::os::raw::c_short;
/// A Java Float type mapping
pub type JavaFloat = f32;
/// A Java Double type mapping
pub type JavaDouble = f64;
/// A Java Size type mapping
pub type JavaSize = JavaInteger;

/// A Java Object type mapping
pub type JavaObject = *mut _JavaObject;
/// A Java Class type mapping
pub type JavaClass = JavaObject;
/// A Java Throwable type mapping
pub type JavaThrowable = JavaObject;
/// A Java String type mapping
pub type JavaStringObj = JavaObject;
/// A Java Array type mapping
pub type JavaArray = JavaObject;
/// A Java Boolean Array type mapping
pub type JavaBooleanArray = JavaArray;
/// A Java Byte Array type mapping
pub type JavaByteArray = JavaArray;
/// A Java Char Array type mapping
pub type JavaCharArray = JavaArray;
/// A Java Short Array type mapping
pub type JavaShortArray = JavaArray;
/// A Java Integer Array type mapping
pub type JavaIntegerArray = JavaArray;
/// A Java Long Array type mapping
pub type JavaLongArray = JavaArray;
/// A Java Float Array type mapping
pub type JavaFloatArray = JavaArray;
/// A Java Double Array type mapping
pub type JavaDoubleArray = JavaArray;
/// A Java Object Array type mapping
pub type JavaObjectArray = JavaArray;
/// A Java Weak type mapping
pub type JavaWeak = JavaObject;

/// A Java field id type mapping
pub type JavaFieldId = *mut _JavaFieldId;
/// A Java method id type mapping
pub type JavaMethodId = *mut _JavaMethodId;

/// A JNI Environment type mapping
pub type JNIEnv = *const JNINativeInterface_;
