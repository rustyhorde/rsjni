// Copyright (c) 2017 rsjni developers
//
// Licensed under the Apache License, Version 2.0
// <LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0> or the MIT
// license <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. All files in the project carrying such notice may not be copied,
// modified, or distributed except according to those terms.

//! Java FFI Bindings
pub mod constants;
#[cfg(feature = "eight")]
mod eight;
pub mod enums;
pub mod externs;
#[cfg(feature = "nine")]
mod nine;
#[cfg(feature = "ten")]
mod ten;

pub mod opaque;
pub mod structs;
pub mod union;

#[cfg(feature = "eight")]
pub use ffi::eight::jni;
#[cfg(feature = "nine")]
pub use ffi::nine::jni;
#[cfg(feature = "ten")]
pub use ffi::ten::jni;
