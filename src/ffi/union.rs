// Copyright (c) 2017 rsjni developers
//
// Licensed under the Apache License, Version 2.0
// <LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0> or the MIT
// license <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. All files in the project carrying such notice may not be copied,
// modified, or distributed except according to those terms.

//! Java FFI Unions
use ffi::constants::{JavaBoolean, JavaByte, JavaChar, JavaDouble, JavaFloat, JavaInteger, JavaLong, JavaObject, JavaShort};

#[repr(C)]
#[derive(Copy)]
/// A Java value union of all of the possible Java types.
pub union JavaValue {
    /// A boolean value
    pub boolean: JavaBoolean,
    /// A byte value
    pub byte: JavaByte,
    /// A character value
    pub character: JavaChar,
    /// A short value
    pub short: JavaShort,
    /// A integer value
    pub integer: JavaInteger,
    /// A long value
    pub long: JavaLong,
    /// A float value
    pub float: JavaFloat,
    /// A double value
    pub double: JavaDouble,
    /// An Object value
    pub object: JavaObject,
    #[cfg(target_pointer_width = "64")]
    #[doc(hidden)]
    _bindgen_union_align: u64,
    #[cfg(target_pointer_width = "32")]
    #[doc(hidden)]
    _bindgen_union_align: [u32; 2_usize],
}

impl Clone for JavaValue {
    fn clone(&self) -> Self {
        *self
    }
}

impl Default for JavaValue {
    fn default() -> Self {
        unsafe { ::core::mem::zeroed() }
    }
}

impl ::std::fmt::Debug for JavaValue {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        write!(f, "JavaValue {{ union }}")
    }
}
