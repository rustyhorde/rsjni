// Copyright (c) 2017 rsjni developers
//
// Licensed under the Apache License, Version 2.0
// <LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0> or the MIT
// license <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. All files in the project carrying such notice may not be copied,
// modified, or distributed except according to those terms.

//! Java FFI Enums

#[repr(u32)]
#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
/// The Java object ref type enum.
#[allow(dead_code)]
pub enum _JavaObjectType {
    /// Invalid
    Invalid = 0,
    /// Local
    Local = 1,
    /// Global
    Global = 2,
    /// Weak Global
    WeakGlobal = 3,
}

pub use self::_JavaObjectType as JavaObjectRefType;
