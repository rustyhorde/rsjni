// Copyright (c) 2017 rsjni developers
//
// Licensed under the Apache License, Version 2.0
// <LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0> or the MIT
// license <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. All files in the project carrying such notice may not be copied,
// modified, or distributed except according to those terms.

//! Java FFI structs (common)
use error::{Error, Result};
use ffi::constants::{JavaBoolean, JavaInteger, JavaObject, JavaVM};
use ffi::jni::JNINativeInterface_;
use jvm::FFIOpts;
use std::convert::TryFrom;
use utils::PrivateTryFromUsize;

#[repr(C)]
#[derive(Debug, Clone, Copy, Hash, PartialEq, Eq)]
/// A `JavaVM_` struct.
pub struct JavaVM_ {
    /// Functions
    pub functions: *const JNIInvokeInterface_,
}

impl Default for JavaVM_ {
    fn default() -> Self {
        unsafe { ::core::mem::zeroed() }
    }
}

#[repr(C)]
#[derive(Debug, Clone, Copy, Hash, PartialEq, Eq)]
/// Java Virtual Machine Attach Arguments
pub struct JavaVMAttachArgs {
    /// Version
    pub version: JavaInteger,
    /// Name
    pub name: *mut ::std::os::raw::c_char,
    /// Group
    pub group: JavaObject,
}

impl Default for JavaVMAttachArgs {
    fn default() -> Self {
        unsafe { ::core::mem::zeroed() }
    }
}

#[repr(C)]
#[derive(Debug, Clone, Copy, Hash, PartialEq, Eq)]
/// Java Virtual Machine Init Arguments
pub struct JavaVMInitArgs {
    /// Version
    pub version: JavaInteger,
    /// Number of options
    pub num_options: JavaInteger,
    /// Option arguments
    pub options: *mut JavaVMOption,
    /// Ignore unrecognized options.
    pub ignore_unrecognized: JavaBoolean,
}

impl Default for JavaVMInitArgs {
    fn default() -> Self {
        unsafe { ::core::mem::zeroed() }
    }
}

impl<'a> TryFrom<&'a mut FFIOpts> for JavaVMInitArgs {
    type Error = Error;

    fn try_from(opts: &mut FFIOpts) -> Result<Self> {
        Ok(Self {
            version: (&opts.version).into(),
            num_options: i32::private_try_from(opts.options.len())?,
            options: opts.options.as_mut_ptr(),
            ignore_unrecognized: if opts.ignore_unrecognized { 1_u8 } else { 0_u8 },
        })
    }
}

#[repr(C)]
#[derive(Debug, Clone, Copy, Hash, PartialEq, Eq)]
/// Java VM Options
pub struct JavaVMOption {
    /// An option string.
    pub option_string: *const ::std::os::raw::c_char,
    /// Extra information.
    pub extra_info: *const ::std::os::raw::c_void,
}

impl Default for JavaVMOption {
    fn default() -> Self {
        unsafe { ::core::mem::zeroed() }
    }
}

#[repr(C)]
#[derive(Debug, Clone, Copy, Hash, PartialEq, Eq)]
/// A `JNIEnv` struct.
pub struct JNIEnv_ {
    /// Funcitons
    pub functions: *const JNINativeInterface_,
}

impl Default for JNIEnv_ {
    fn default() -> Self {
        unsafe { ::core::mem::zeroed() }
    }
}

unsafe impl Sync for JNIInvokeInterface_ {}

#[repr(C)]
#[derive(Debug, Clone, Copy, Hash, PartialEq, Eq)]
/// A `JNIInvokeInterface` struct.
#[cfg_attr(rustfmt, rustfmt_skip)]
pub struct JNIInvokeInterface_ {
    #[doc(hidden)] pub reserved0: *mut ::std::os::raw::c_void,
    #[doc(hidden)] pub reserved1: *mut ::std::os::raw::c_void,
    #[doc(hidden)] pub reserved2: *mut ::std::os::raw::c_void,

    #[doc(hidden)] pub destroy_java_vm: unsafe extern "C" fn(vm: *mut JavaVM) -> JavaInteger,
    #[doc(hidden)] pub attach_current_thread: unsafe extern "C" fn(
        vm: *mut JavaVM,
        penv: *mut *mut ::std::os::raw::c_void,
        args: *mut ::std::os::raw::c_void
    ) -> JavaInteger,
    #[doc(hidden)] pub detach_current_thread: unsafe extern "C" fn(vm: *mut JavaVM) -> JavaInteger,
    #[doc(hidden)] pub get_env: unsafe extern "C" fn(vm: *mut JavaVM, penv: *mut *mut ::std::os::raw::c_void, version: JavaInteger) -> JavaInteger,
    #[doc(hidden)] pub attach_current_thread_as_daemon: unsafe extern "C" fn(
        vm: *mut JavaVM,
        penv: *mut *mut ::std::os::raw::c_void,
        args: *mut ::std::os::raw::c_void
    ) -> JavaInteger,
}

impl Default for JNIInvokeInterface_ {
    fn default() -> Self {
        unsafe { ::core::mem::zeroed() }
    }
}

#[repr(C)]
#[derive(Debug, Clone, Copy, Hash, PartialEq, Eq)]
/// A `JNINativeMethod` struct.
pub struct JNINativeMethod {
    /// The method name.
    pub name: *mut ::std::os::raw::c_char,
    /// The method signature.
    pub signature: *mut ::std::os::raw::c_char,
    /// A function pointer.
    pub fn_ptr: *mut ::std::os::raw::c_void,
}

impl Default for JNINativeMethod {
    fn default() -> Self {
        unsafe { ::core::mem::zeroed() }
    }
}
