// Copyright (c) 2017 rsjni developers
//
// Licensed under the Apache License, Version 2.0
// <LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0> or the MIT
// license <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. All files in the project carrying such notice may not be copied,
// modified, or distributed except according to those terms.

//! Java Externs
use ffi::constants::{JNIEnv, JavaInteger, JavaSize, JavaVM};
use ffi::structs::JavaVMInitArgs;

extern "C" {
    #[allow(dead_code)]
    pub fn JNI_GetDefaultJavaVMInitArgs(args: *mut JavaVMInitArgs) -> JavaInteger;
    pub fn JNI_CreateJavaVM(pvm: *mut *mut JavaVM, penv: *mut *mut JNIEnv, args: *mut JavaVMInitArgs) -> JavaInteger;
    #[allow(dead_code)]
    pub fn JNI_GetCreatedJavaVMs(arg1: *mut *mut JavaVM, arg2: JavaSize, arg3: *mut JavaSize) -> JavaInteger;
    #[allow(dead_code)]
    pub fn JNI_OnLoad(vm: *mut JavaVM, reserved: *mut ::std::os::raw::c_void) -> JavaInteger;
    #[allow(dead_code)]
    pub fn JNI_OnUnload(vm: *mut JavaVM, reserved: *mut ::std::os::raw::c_void);
}
