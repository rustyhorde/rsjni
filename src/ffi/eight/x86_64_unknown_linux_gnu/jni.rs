// Copyright (c) 2017 rsjni developers
//
// Licensed under the Apache License, Version 2.0
// <LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0> or the MIT
// license <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. All files in the project carrying such notice may not be copied,
// modified, or distributed except according to those terms.

//! Java 8 FFI structs
use ffi::constants::{
    JNIEnv, JavaArray, JavaBoolean, JavaBooleanArray, JavaByte, JavaByteArray, JavaChar, JavaCharArray, JavaClass, JavaDouble, JavaDoubleArray, JavaFieldId, JavaFloat, JavaFloatArray, JavaInteger,
    JavaIntegerArray, JavaLong, JavaLongArray, JavaMethodId, JavaObject, JavaObjectArray, JavaShort, JavaShortArray, JavaSize, JavaStringObj, JavaThrowable, JavaVM, JavaWeak,
};
use ffi::enums::JavaObjectRefType;
use ffi::structs::JNINativeMethod;
use ffi::union::JavaValue;

#[repr(C)]
#[derive(Debug, Clone, Copy, Hash, PartialEq, Eq)]
/// A `JNINativeInterface` struct
#[cfg_attr(rustfmt, rustfmt_skip)]
pub struct JNINativeInterface_ {
    #[doc(hidden)] pub reserved0: *mut ::std::os::raw::c_void,
    #[doc(hidden)] pub reserved1: *mut ::std::os::raw::c_void,
    #[doc(hidden)] pub reserved2: *mut ::std::os::raw::c_void,
    #[doc(hidden)] pub reserved3: *mut ::std::os::raw::c_void,

    // It's important all these functions stay in the order that they're
	// currently in, and that none are commented out, because each function
	// pointer corresponds to its original in the JNINativeInterface struct in
	// the `jni.h` header file.
	//
	// Since the Rust/C interface here relies on the location of each struct
	// field in memory, its important that the two versions of the struct
	// correspond.
    #[doc(hidden)] pub get_version: unsafe extern "C" fn(env: *mut JNIEnv) -> JavaInteger,

    #[doc(hidden)] pub define_class: unsafe extern "C" fn(env: *mut JNIEnv, name: *const ::std::os::raw::c_char, loader: JavaObject, buf: *const JavaByte, len: JavaSize) -> JavaClass,
    #[doc(hidden)] pub find_class: unsafe extern "C" fn(env: *mut JNIEnv, name: *const ::std::os::raw::c_char) -> JavaClass,

    #[doc(hidden)] pub from_reflected_method: unsafe extern "C" fn(env: *mut JNIEnv, method: JavaObject) -> JavaMethodId,
    #[doc(hidden)] pub from_reflected_field: unsafe extern "C" fn(env: *mut JNIEnv, field: JavaObject) -> JavaFieldId,
    #[doc(hidden)] pub to_reflected_method: unsafe extern "C" fn(env: *mut JNIEnv, cls: JavaClass, methodID: JavaMethodId, isStatic: JavaBoolean) -> JavaObject,

    #[doc(hidden)] pub get_superclass: unsafe extern "C" fn(env: *mut JNIEnv, sub: JavaClass) -> JavaClass,
    #[doc(hidden)] pub is_assignable_from: unsafe extern "C" fn(env: *mut JNIEnv, sub: JavaClass, sup: JavaClass) -> JavaBoolean,

    #[doc(hidden)] pub to_reflected_field: unsafe extern "C" fn(env: *mut JNIEnv, cls: JavaClass, fieldID: JavaFieldId, isStatic: JavaBoolean) -> JavaObject,

    #[doc(hidden)] pub throw: unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaThrowable) -> JavaInteger,
    #[doc(hidden)] pub throw_new: unsafe extern "C" fn(env: *mut JNIEnv, clazz: JavaClass, msg: *const ::std::os::raw::c_char) -> JavaInteger,
    #[doc(hidden)] pub exception_ocurred: unsafe extern "C" fn(env: *mut JNIEnv) -> JavaThrowable,
    #[doc(hidden)] pub exception_describe: unsafe extern "C" fn(env: *mut JNIEnv),
    #[doc(hidden)] pub exception_clear: unsafe extern "C" fn(env: *mut JNIEnv),
    #[doc(hidden)] pub fatal_error: unsafe extern "C" fn(env: *mut JNIEnv, msg: *const ::std::os::raw::c_char),

    #[doc(hidden)] pub push_local_frame: unsafe extern "C" fn(env: *mut JNIEnv, capacity: JavaInteger) -> JavaInteger,
    #[doc(hidden)] pub pop_local_frame: unsafe extern "C" fn(env: *mut JNIEnv, result: JavaObject) -> JavaObject,
    #[doc(hidden)] pub new_global_ref: unsafe extern "C" fn(env: *mut JNIEnv, lobj: JavaObject) -> JavaObject,
    #[doc(hidden)] pub delete_global_ref: unsafe extern "C" fn(env: *mut JNIEnv, gref: JavaObject),
    #[doc(hidden)] pub delete_local_ref: unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject),
    #[doc(hidden)] pub is_same_object: unsafe extern "C" fn(env: *mut JNIEnv, obj1: JavaObject, obj2: JavaObject) -> JavaBoolean,
    #[doc(hidden)] pub new_local_ref: unsafe extern "C" fn(env: *mut JNIEnv, ref_: JavaObject) -> JavaObject,
    #[doc(hidden)] pub ensure_local_capacity: unsafe extern "C" fn(env: *mut JNIEnv, capacity: JavaInteger) -> JavaInteger,

    #[doc(hidden)] pub alloc_object: unsafe extern "C" fn(env: *mut JNIEnv, clazz: JavaClass) -> JavaObject,
    #[doc(hidden)] pub new_object: unsafe extern "C" fn(env: *mut JNIEnv, clazz: JavaClass, methodID: JavaMethodId, ...) -> JavaObject,
    #[doc(hidden)] pub new_object_v: unsafe extern "C" fn(env: *mut JNIEnv, clazz: JavaClass, methodID: JavaMethodId, args: *mut __va_list_tag) -> JavaObject,
    #[doc(hidden)] pub new_object_a: unsafe extern "C" fn(env: *mut JNIEnv, clazz: JavaClass, methodID: JavaMethodId, args: *const JavaValue) -> JavaObject,

    #[doc(hidden)] pub get_object_class: unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject) -> JavaClass,
    #[doc(hidden)] pub is_instance_of: unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, clazz: JavaClass) -> JavaBoolean,

    #[doc(hidden)] pub get_method_id: unsafe extern "C" fn(env: *mut JNIEnv, clazz: JavaClass, name: *const ::std::os::raw::c_char, sig: *const ::std::os::raw::c_char) -> JavaMethodId,

    #[doc(hidden)] pub call_object_method: unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, methodID: JavaMethodId, ...) -> JavaObject,
    #[doc(hidden)] pub call_object_method_v: unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, methodID: JavaMethodId, args: *mut __va_list_tag) -> JavaObject,
    #[doc(hidden)] pub call_object_method_a: unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, methodID: JavaMethodId, args: *const JavaValue) -> JavaObject,
    #[doc(hidden)] pub call_boolean_method: unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, methodID: JavaMethodId, ...) -> JavaBoolean,
    #[doc(hidden)] pub call_boolean_method_v: unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, methodID: JavaMethodId, args: *mut __va_list_tag) -> JavaBoolean,
    #[doc(hidden)] pub call_boolean_method_a: unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, methodID: JavaMethodId, args: *const JavaValue) -> JavaBoolean,
    #[doc(hidden)] pub call_byte_method: unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, methodID: JavaMethodId, ...) -> JavaByte,
    #[doc(hidden)] pub call_byte_method_v: unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, methodID: JavaMethodId, args: *mut __va_list_tag) -> JavaByte,
    #[doc(hidden)] pub call_byte_method_a: unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, methodID: JavaMethodId, args: *const JavaValue) -> JavaByte,
    #[doc(hidden)] pub call_char_method: unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, methodID: JavaMethodId, ...) -> JavaChar,
    #[doc(hidden)] pub call_char_method_v: unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, methodID: JavaMethodId, args: *mut __va_list_tag) -> JavaChar,
    #[doc(hidden)] pub call_char_method_a: unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, methodID: JavaMethodId, args: *const JavaValue) -> JavaChar,
    #[doc(hidden)] pub call_short_method: unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, methodID: JavaMethodId, ...) -> JavaShort,
    #[doc(hidden)] pub call_short_method_v: unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, methodID: JavaMethodId, args: *mut __va_list_tag) -> JavaShort,
    #[doc(hidden)] pub call_short_method_a: unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, methodID: JavaMethodId, args: *const JavaValue) -> JavaShort,
    #[doc(hidden)] pub call_int_method: unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, methodID: JavaMethodId, ...) -> JavaInteger,
    #[doc(hidden)] pub call_int_method_v: unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, methodID: JavaMethodId, args: *mut __va_list_tag) -> JavaInteger,
    #[doc(hidden)] pub call_int_method_a: unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, methodID: JavaMethodId, args: *const JavaValue) -> JavaInteger,
    #[doc(hidden)] pub call_long_method: unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, methodID: JavaMethodId, ...) -> JavaLong,
    #[doc(hidden)] pub call_long_method_v: unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, methodID: JavaMethodId, args: *mut __va_list_tag) -> JavaLong,
    #[doc(hidden)] pub call_long_method_a: unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, methodID: JavaMethodId, args: *const JavaValue) -> JavaLong,
    #[doc(hidden)] pub call_float_method: unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, methodID: JavaMethodId, ...) -> JavaFloat,
    #[doc(hidden)] pub call_float_method_v: unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, methodID: JavaMethodId, args: *mut __va_list_tag) -> JavaFloat,
    #[doc(hidden)] pub call_float_method_a: unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, methodID: JavaMethodId, args: *const JavaValue) -> JavaFloat,
    #[doc(hidden)] pub call_double_method: unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, methodID: JavaMethodId, ...) -> JavaDouble,
    #[doc(hidden)] pub call_double_method_v: unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, methodID: JavaMethodId, args: *mut __va_list_tag) -> JavaDouble,
    #[doc(hidden)] pub call_double_method_a: unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, methodID: JavaMethodId, args: *const JavaValue) -> JavaDouble,
    #[doc(hidden)] pub call_void_method: unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, methodID: JavaMethodId, ...),
    #[doc(hidden)] pub call_void_method_v: unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, methodID: JavaMethodId, args: *mut __va_list_tag),
    #[doc(hidden)] pub call_void_method_a: unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, methodID: JavaMethodId, args: *const JavaValue),

    #[doc(hidden)] pub call_nonvirtual_object_method: unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, clazz: JavaClass, methodID: JavaMethodId, ...) -> JavaObject,
    #[doc(hidden)] pub call_nonvirtual_object_method_v: unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, clazz: JavaClass, methodID: JavaMethodId, args: *mut __va_list_tag) -> JavaObject,
    #[doc(hidden)] pub call_nonvirtual_object_method_a:  unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, clazz: JavaClass, methodID: JavaMethodId, args: *const JavaValue) -> JavaObject,
    #[doc(hidden)] pub call_nonvirtual_boolean_method: unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, clazz: JavaClass, methodID: JavaMethodId, ...) -> JavaBoolean,
    #[doc(hidden)] pub call_nonvirtual_boolean_method_v:  unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, clazz: JavaClass, methodID: JavaMethodId, args: *mut __va_list_tag) -> JavaBoolean,
    #[doc(hidden)] pub call_nonvirtual_boolean_method_a:  unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, clazz: JavaClass, methodID: JavaMethodId, args: *const JavaValue) -> JavaBoolean,
    #[doc(hidden)] pub call_nonvirtual_byte_method: unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, clazz: JavaClass, methodID: JavaMethodId, ...) -> JavaByte,
    #[doc(hidden)] pub call_nonvirtual_byte_method_v:  unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, clazz: JavaClass, methodID: JavaMethodId, args: *mut __va_list_tag) -> JavaByte,
    #[doc(hidden)] pub call_nonvirtual_byte_method_a:  unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, clazz: JavaClass, methodID: JavaMethodId, args: *const JavaValue) -> JavaByte,
    #[doc(hidden)] pub call_nonvirtual_char_method: unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, clazz: JavaClass, methodID: JavaMethodId, ...) -> JavaChar,
    #[doc(hidden)] pub call_nonvirtual_char_method_v:  unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, clazz: JavaClass, methodID: JavaMethodId, args: *mut __va_list_tag) -> JavaChar,
    #[doc(hidden)] pub call_nonvirtual_char_method_a:  unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, clazz: JavaClass, methodID: JavaMethodId, args: *const JavaValue) -> JavaChar,
    #[doc(hidden)] pub call_nonvirtual_short_method: unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, clazz: JavaClass, methodID: JavaMethodId, ...) -> JavaShort,
    #[doc(hidden)] pub call_nonvirtual_short_method_v:  unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, clazz: JavaClass, methodID: JavaMethodId, args: *mut __va_list_tag) -> JavaShort,
    #[doc(hidden)] pub call_nonvirtual_short_method_a:  unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, clazz: JavaClass, methodID: JavaMethodId, args: *const JavaValue) -> JavaShort,
    #[doc(hidden)] pub call_nonvirtual_int_method: unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, clazz: JavaClass, methodID: JavaMethodId, ...) -> JavaInteger,
    #[doc(hidden)] pub call_nonvirtual_int_method_v:  unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, clazz: JavaClass, methodID: JavaMethodId, args: *mut __va_list_tag) -> JavaInteger,
    #[doc(hidden)] pub call_nonvirtual_int_method_a:  unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, clazz: JavaClass, methodID: JavaMethodId, args: *const JavaValue) -> JavaInteger,
    #[doc(hidden)] pub call_nonvirtual_long_method: unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, clazz: JavaClass, methodID: JavaMethodId, ...) -> JavaLong,
    #[doc(hidden)] pub call_nonvirtual_long_method_v:  unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, clazz: JavaClass, methodID: JavaMethodId, args: *mut __va_list_tag) -> JavaLong,
    #[doc(hidden)] pub call_nonvirtual_long_method_a:  unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, clazz: JavaClass, methodID: JavaMethodId, args: *const JavaValue) -> JavaLong,
    #[doc(hidden)] pub call_nonvirtual_float_method: unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, clazz: JavaClass, methodID: JavaMethodId, ...) -> JavaFloat,
    #[doc(hidden)] pub call_nonvirtual_float_method_v:  unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, clazz: JavaClass, methodID: JavaMethodId, args: *mut __va_list_tag) -> JavaFloat,
    #[doc(hidden)] pub call_nonvirtual_float_method_a:  unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, clazz: JavaClass, methodID: JavaMethodId, args: *const JavaValue) -> JavaFloat,
    #[doc(hidden)] pub call_nonvirtual_double_method: unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, clazz: JavaClass, methodID: JavaMethodId, ...) -> JavaDouble,
    #[doc(hidden)] pub call_nonvirtual_double_method_v:  unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, clazz: JavaClass, methodID: JavaMethodId, args: *mut __va_list_tag) -> JavaDouble,
    #[doc(hidden)] pub call_nonvirtual_double_method_a:  unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, clazz: JavaClass, methodID: JavaMethodId, args: *const JavaValue) -> JavaDouble,
    #[doc(hidden)] pub call_nonvirtual_void_method: unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, clazz: JavaClass, methodID: JavaMethodId, ...),
    #[doc(hidden)] pub call_nonvirtual_void_method_v: unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, clazz: JavaClass, methodID: JavaMethodId, args: *mut __va_list_tag),
    #[doc(hidden)] pub call_nonvirtual_void_method_a: unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, clazz: JavaClass, methodID: JavaMethodId, args: *const JavaValue),

    #[doc(hidden)] pub get_field_id: unsafe extern "C" fn(env: *mut JNIEnv, clazz: JavaClass, name: *const ::std::os::raw::c_char, sig: *const ::std::os::raw::c_char) -> JavaFieldId,

    #[doc(hidden)] pub get_object_field: unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, fieldID: JavaFieldId) -> JavaObject,
    #[doc(hidden)] pub get_boolean_field: unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, fieldID: JavaFieldId) -> JavaBoolean,
    #[doc(hidden)] pub get_byte_field: unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, fieldID: JavaFieldId) -> JavaByte,
    #[doc(hidden)] pub get_char_field: unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, fieldID: JavaFieldId) -> JavaChar,
    #[doc(hidden)] pub get_short_field: unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, fieldID: JavaFieldId) -> JavaShort,
    #[doc(hidden)] pub get_int_field: unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, fieldID: JavaFieldId) -> JavaInteger,
    #[doc(hidden)] pub get_long_field: unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, fieldID: JavaFieldId) -> JavaLong,
    #[doc(hidden)] pub get_float_field: unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, fieldID: JavaFieldId) -> JavaFloat,
    #[doc(hidden)] pub get_double_field: unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, fieldID: JavaFieldId) -> JavaDouble,

    #[doc(hidden)] pub set_object_field: unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, fieldID: JavaFieldId, val: JavaObject),
    #[doc(hidden)] pub set_boolean_field: unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, fieldID: JavaFieldId, val: JavaBoolean),
    #[doc(hidden)] pub set_byte_field: unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, fieldID: JavaFieldId, val: JavaByte),
    #[doc(hidden)] pub set_char_field: unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, fieldID: JavaFieldId, val: JavaChar),
    #[doc(hidden)] pub set_short_field: unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, fieldID: JavaFieldId, val: JavaShort),
    #[doc(hidden)] pub set_int_field: unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, fieldID: JavaFieldId, val: JavaInteger),
    #[doc(hidden)] pub set_long_field: unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, fieldID: JavaFieldId, val: JavaLong),
    #[doc(hidden)] pub set_float_field: unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, fieldID: JavaFieldId, val: JavaFloat),
    #[doc(hidden)] pub set_double_field: unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject, fieldID: JavaFieldId, val: JavaDouble),

    #[doc(hidden)] pub get_static_method_id: unsafe extern "C" fn(env: *mut JNIEnv, clazz: JavaClass, name: *const ::std::os::raw::c_char, sig: *const ::std::os::raw::c_char) -> JavaMethodId,

    #[doc(hidden)] pub call_static_object_method: unsafe extern "C" fn(env: *mut JNIEnv, clazz: JavaClass, methodID: JavaMethodId, ...) -> JavaObject,
    #[doc(hidden)] pub call_static_object_method_v: unsafe extern "C" fn(env: *mut JNIEnv, clazz: JavaClass, methodID: JavaMethodId, args: *mut __va_list_tag) -> JavaObject,
    #[doc(hidden)] pub call_static_object_method_a: unsafe extern "C" fn(env: *mut JNIEnv, clazz: JavaClass, methodID: JavaMethodId, args: *const JavaValue) -> JavaObject,
    #[doc(hidden)] pub call_static_boolean_method: unsafe extern "C" fn(env: *mut JNIEnv, clazz: JavaClass, methodID: JavaMethodId, ...) -> JavaBoolean,
    #[doc(hidden)] pub call_static_boolean_method_v: unsafe extern "C" fn(env: *mut JNIEnv, clazz: JavaClass, methodID: JavaMethodId, args: *mut __va_list_tag) -> JavaBoolean,
    #[doc(hidden)] pub call_static_boolean_method_a: unsafe extern "C" fn(env: *mut JNIEnv, clazz: JavaClass, methodID: JavaMethodId, args: *const JavaValue) -> JavaBoolean,
    #[doc(hidden)] pub call_static_byte_method: unsafe extern "C" fn(env: *mut JNIEnv, clazz: JavaClass, methodID: JavaMethodId, ...) -> JavaByte,
    #[doc(hidden)] pub call_static_byte_method_v: unsafe extern "C" fn(env: *mut JNIEnv, clazz: JavaClass, methodID: JavaMethodId, args: *mut __va_list_tag) -> JavaByte,
    #[doc(hidden)] pub call_static_byte_method_a: unsafe extern "C" fn(env: *mut JNIEnv, clazz: JavaClass, methodID: JavaMethodId, args: *const JavaValue) -> JavaByte,
    #[doc(hidden)] pub call_static_char_method: unsafe extern "C" fn(env: *mut JNIEnv, clazz: JavaClass, methodID: JavaMethodId, ...) -> JavaChar,
    #[doc(hidden)] pub call_static_char_method_v: unsafe extern "C" fn(env: *mut JNIEnv, clazz: JavaClass, methodID: JavaMethodId, args: *mut __va_list_tag) -> JavaChar,
    #[doc(hidden)] pub call_static_char_method_a: unsafe extern "C" fn(env: *mut JNIEnv, clazz: JavaClass, methodID: JavaMethodId, args: *const JavaValue) -> JavaChar,
    #[doc(hidden)] pub call_static_short_method: unsafe extern "C" fn(env: *mut JNIEnv, clazz: JavaClass, methodID: JavaMethodId, ...) -> JavaShort,
    #[doc(hidden)] pub call_static_short_method_v: unsafe extern "C" fn(env: *mut JNIEnv, clazz: JavaClass, methodID: JavaMethodId, args: *mut __va_list_tag) -> JavaShort,
    #[doc(hidden)] pub call_static_short_method_a: unsafe extern "C" fn(env: *mut JNIEnv, clazz: JavaClass, methodID: JavaMethodId, args: *const JavaValue) -> JavaShort,
    #[doc(hidden)] pub call_static_int_method: unsafe extern "C" fn(env: *mut JNIEnv, clazz: JavaClass, methodID: JavaMethodId, ...) -> JavaInteger,
    #[doc(hidden)] pub call_static_int_method_v: unsafe extern "C" fn(env: *mut JNIEnv, clazz: JavaClass, methodID: JavaMethodId, args: *mut __va_list_tag) -> JavaInteger,
    #[doc(hidden)] pub call_static_int_method_a: unsafe extern "C" fn(env: *mut JNIEnv, clazz: JavaClass, methodID: JavaMethodId, args: *const JavaValue) -> JavaInteger,
    #[doc(hidden)] pub call_static_long_method: unsafe extern "C" fn(env: *mut JNIEnv, clazz: JavaClass, methodID: JavaMethodId, ...) -> JavaLong,
    #[doc(hidden)] pub call_static_long_method_v: unsafe extern "C" fn(env: *mut JNIEnv, clazz: JavaClass, methodID: JavaMethodId, args: *mut __va_list_tag) -> JavaLong,
    #[doc(hidden)] pub call_static_long_method_a: unsafe extern "C" fn(env: *mut JNIEnv, clazz: JavaClass, methodID: JavaMethodId, args: *const JavaValue) -> JavaLong,
    #[doc(hidden)] pub call_static_float_method: unsafe extern "C" fn(env: *mut JNIEnv, clazz: JavaClass, methodID: JavaMethodId, ...) -> JavaFloat,
    #[doc(hidden)] pub call_static_float_method_v: unsafe extern "C" fn(env: *mut JNIEnv, clazz: JavaClass, methodID: JavaMethodId, args: *mut __va_list_tag) -> JavaFloat,
    #[doc(hidden)] pub call_static_float_method_a: unsafe extern "C" fn(env: *mut JNIEnv, clazz: JavaClass, methodID: JavaMethodId, args: *const JavaValue) -> JavaFloat,
    #[doc(hidden)] pub call_static_double_method: unsafe extern "C" fn(env: *mut JNIEnv, clazz: JavaClass, methodID: JavaMethodId, ...) -> JavaDouble,
    #[doc(hidden)] pub call_static_double_method_v: unsafe extern "C" fn(env: *mut JNIEnv, clazz: JavaClass, methodID: JavaMethodId, args: *mut __va_list_tag) -> JavaDouble,
    #[doc(hidden)] pub call_static_double_method_a: unsafe extern "C" fn(env: *mut JNIEnv, clazz: JavaClass, methodID: JavaMethodId, args: *const JavaValue) -> JavaDouble,
    #[doc(hidden)] pub call_static_void_method: unsafe extern "C" fn(env: *mut JNIEnv, cls: JavaClass, methodID: JavaMethodId, ...),
    #[doc(hidden)] pub call_static_void_method_v: unsafe extern "C" fn(env: *mut JNIEnv, cls: JavaClass, methodID: JavaMethodId, args: *mut __va_list_tag),
    #[doc(hidden)] pub call_static_void_method_a: unsafe extern "C" fn(env: *mut JNIEnv, cls: JavaClass, methodID: JavaMethodId, args: *const JavaValue),

    #[doc(hidden)] pub get_static_field_id: unsafe extern "C" fn(env: *mut JNIEnv, clazz: JavaClass, name: *const ::std::os::raw::c_char, sig: *const ::std::os::raw::c_char) -> JavaFieldId,

    #[doc(hidden)] pub get_static_object_field: unsafe extern "C" fn(env: *mut JNIEnv, clazz: JavaClass, fieldID: JavaFieldId) -> JavaObject,
    #[doc(hidden)] pub get_static_boolean_field: unsafe extern "C" fn(env: *mut JNIEnv, clazz: JavaClass, fieldID: JavaFieldId) -> JavaBoolean,
    #[doc(hidden)] pub get_static_byte_field: unsafe extern "C" fn(env: *mut JNIEnv, clazz: JavaClass, fieldID: JavaFieldId) -> JavaByte,
    #[doc(hidden)] pub get_static_char_field: unsafe extern "C" fn(env: *mut JNIEnv, clazz: JavaClass, fieldID: JavaFieldId) -> JavaChar,
    #[doc(hidden)] pub get_static_short_field: unsafe extern "C" fn(env: *mut JNIEnv, clazz: JavaClass, fieldID: JavaFieldId) -> JavaShort,
    #[doc(hidden)] pub get_static_int_field: unsafe extern "C" fn(env: *mut JNIEnv, clazz: JavaClass, fieldID: JavaFieldId) -> JavaInteger,
    #[doc(hidden)] pub get_static_long_field: unsafe extern "C" fn(env: *mut JNIEnv, clazz: JavaClass, fieldID: JavaFieldId) -> JavaLong,
    #[doc(hidden)] pub get_static_float_field: unsafe extern "C" fn(env: *mut JNIEnv, clazz: JavaClass, fieldID: JavaFieldId) -> JavaFloat,
    #[doc(hidden)] pub get_static_double_field: unsafe extern "C" fn(env: *mut JNIEnv, clazz: JavaClass, fieldID: JavaFieldId) -> JavaDouble,

    #[doc(hidden)] pub set_static_object_field: unsafe extern "C" fn(env: *mut JNIEnv, clazz: JavaClass, fieldID: JavaFieldId, value: JavaObject),
    #[doc(hidden)] pub set_static_boolean_field: unsafe extern "C" fn(env: *mut JNIEnv, clazz: JavaClass, fieldID: JavaFieldId, value: JavaBoolean),
    #[doc(hidden)] pub set_static_byte_field: unsafe extern "C" fn(env: *mut JNIEnv, clazz: JavaClass, fieldID: JavaFieldId, value: JavaByte),
    #[doc(hidden)] pub set_static_char_field: unsafe extern "C" fn(env: *mut JNIEnv, clazz: JavaClass, fieldID: JavaFieldId, value: JavaChar),
    #[doc(hidden)] pub set_static_short_field: unsafe extern "C" fn(env: *mut JNIEnv, clazz: JavaClass, fieldID: JavaFieldId, value: JavaShort),
    #[doc(hidden)] pub set_static_int_field: unsafe extern "C" fn(env: *mut JNIEnv, clazz: JavaClass, fieldID: JavaFieldId, value: JavaInteger),
    #[doc(hidden)] pub set_static_long_field: unsafe extern "C" fn(env: *mut JNIEnv, clazz: JavaClass, fieldID: JavaFieldId, value: JavaLong),
    #[doc(hidden)] pub set_static_float_field: unsafe extern "C" fn(env: *mut JNIEnv, clazz: JavaClass, fieldID: JavaFieldId, value: JavaFloat),
    #[doc(hidden)] pub set_static_double_field: unsafe extern "C" fn(env: *mut JNIEnv, clazz: JavaClass, fieldID: JavaFieldId, value: JavaDouble),

    #[doc(hidden)] pub new_string: unsafe extern "C" fn(env: *mut JNIEnv, unicode: *const JavaChar, len: JavaSize) -> JavaStringObj,
    #[doc(hidden)] pub get_string_length: unsafe extern "C" fn(env: *mut JNIEnv, str: JavaStringObj) -> JavaSize,
    #[doc(hidden)] pub get_string_chars: unsafe extern "C" fn(env: *mut JNIEnv, str: JavaStringObj, isCopy: *mut JavaBoolean) -> *const JavaChar,
    #[doc(hidden)] pub release_string_chars: unsafe extern "C" fn(env: *mut JNIEnv, str: JavaStringObj, chars: *const JavaChar),
    #[doc(hidden)] pub new_string_utf: unsafe extern "C" fn(env: *mut JNIEnv, utf: *const ::std::os::raw::c_char) -> JavaStringObj,
    #[doc(hidden)] pub get_string_utf_length: unsafe extern "C" fn(env: *mut JNIEnv, str: JavaStringObj) -> JavaSize,
    #[doc(hidden)] pub get_string_utf_chars: unsafe extern "C" fn(env: *mut JNIEnv, str: JavaStringObj, isCopy: *mut JavaBoolean) -> *const ::std::os::raw::c_char,
    #[doc(hidden)] pub release_string_utf_chars: unsafe extern "C" fn(env: *mut JNIEnv, str: JavaStringObj, chars: *const ::std::os::raw::c_char),

    #[doc(hidden)] pub get_array_length: unsafe extern "C" fn(env: *mut JNIEnv, array: JavaArray) -> JavaSize,
    #[doc(hidden)] pub new_object_array: unsafe extern "C" fn(env: *mut JNIEnv, len: JavaSize, clazz: JavaClass, init: JavaObject) -> JavaObjectArray,
    #[doc(hidden)] pub get_object_array_element: unsafe extern "C" fn(env: *mut JNIEnv, array: JavaObjectArray, index: JavaSize) -> JavaObject,
    #[doc(hidden)] pub set_object_array_element: unsafe extern "C" fn(env: *mut JNIEnv, array: JavaObjectArray, index: JavaSize, val: JavaObject),

    #[doc(hidden)] pub new_boolean_array: unsafe extern "C" fn(env: *mut JNIEnv, len: JavaSize) -> JavaBooleanArray,
    #[doc(hidden)] pub new_byte_array: unsafe extern "C" fn(env: *mut JNIEnv, len: JavaSize) -> JavaByteArray,
    #[doc(hidden)] pub new_char_array: unsafe extern "C" fn(env: *mut JNIEnv, len: JavaSize) -> JavaCharArray,
    #[doc(hidden)] pub new_short_array: unsafe extern "C" fn(env: *mut JNIEnv, len: JavaSize) -> JavaShortArray,
    #[doc(hidden)] pub new_int_array: unsafe extern "C" fn(env: *mut JNIEnv, len: JavaSize) -> JavaIntegerArray,
    #[doc(hidden)] pub new_long_array: unsafe extern "C" fn(env: *mut JNIEnv, len: JavaSize) -> JavaLongArray,
    #[doc(hidden)] pub new_float_array: unsafe extern "C" fn(env: *mut JNIEnv, len: JavaSize) -> JavaFloatArray,
    #[doc(hidden)] pub new_double_array: unsafe extern "C" fn(env: *mut JNIEnv, len: JavaSize) -> JavaDoubleArray,

    #[doc(hidden)] pub get_boolean_array_elements: unsafe extern "C" fn(env: *mut JNIEnv, array: JavaBooleanArray, isCopy: *mut JavaBoolean) -> *mut JavaBoolean,
    #[doc(hidden)] pub get_byte_array_elements: unsafe extern "C" fn(env: *mut JNIEnv, array: JavaByteArray, isCopy: *mut JavaBoolean) -> *mut JavaByte,
    #[doc(hidden)] pub get_char_array_elements: unsafe extern "C" fn(env: *mut JNIEnv, array: JavaCharArray, isCopy: *mut JavaBoolean) -> *mut JavaChar,
    #[doc(hidden)] pub get_short_array_elements: unsafe extern "C" fn(env: *mut JNIEnv, array: JavaShortArray, isCopy: *mut JavaBoolean) -> *mut JavaShort,
    #[doc(hidden)] pub get_int_array_elements: unsafe extern "C" fn(env: *mut JNIEnv, array: JavaIntegerArray, isCopy: *mut JavaBoolean) -> *mut JavaInteger,
    #[doc(hidden)] pub get_long_array_elements: unsafe extern "C" fn(env: *mut JNIEnv, array: JavaLongArray, isCopy: *mut JavaBoolean) -> *mut JavaLong,
    #[doc(hidden)] pub get_float_array_elements: unsafe extern "C" fn(env: *mut JNIEnv, array: JavaFloatArray, isCopy: *mut JavaBoolean) -> *mut JavaFloat,
    #[doc(hidden)] pub get_double_array_elements: unsafe extern "C" fn(env: *mut JNIEnv, array: JavaDoubleArray, isCopy: *mut JavaBoolean) -> *mut JavaDouble,

    #[doc(hidden)] pub release_boolean_array_elements: unsafe extern "C" fn(env: *mut JNIEnv, array: JavaBooleanArray, elems: *mut JavaBoolean, mode: JavaInteger),
    #[doc(hidden)] pub release_byte_array_elements: unsafe extern "C" fn(env: *mut JNIEnv, array: JavaByteArray, elems: *mut JavaByte, mode: JavaInteger),
    #[doc(hidden)] pub release_char_array_elements: unsafe extern "C" fn(env: *mut JNIEnv, array: JavaCharArray, elems: *mut JavaChar, mode: JavaInteger),
    #[doc(hidden)] pub release_short_array_elements: unsafe extern "C" fn(env: *mut JNIEnv, array: JavaShortArray, elems: *mut JavaShort, mode: JavaInteger),
    #[doc(hidden)] pub release_int_array_elements: unsafe extern "C" fn(env: *mut JNIEnv, array: JavaIntegerArray, elems: *mut JavaInteger, mode: JavaInteger),
    #[doc(hidden)] pub release_long_array_elements: unsafe extern "C" fn(env: *mut JNIEnv, array: JavaLongArray, elems: *mut JavaLong, mode: JavaInteger),
    #[doc(hidden)] pub release_float_array_elements: unsafe extern "C" fn(env: *mut JNIEnv, array: JavaFloatArray, elems: *mut JavaFloat, mode: JavaInteger),
    #[doc(hidden)] pub release_double_array_elements: unsafe extern "C" fn(env: *mut JNIEnv, array: JavaDoubleArray, elems: *mut JavaDouble, mode: JavaInteger),

    #[doc(hidden)] pub get_boolean_array_region: unsafe extern "C" fn(env: *mut JNIEnv, array: JavaBooleanArray, start: JavaSize, l: JavaSize, buf: *mut JavaBoolean),
    #[doc(hidden)] pub get_byte_array_region: unsafe extern "C" fn(env: *mut JNIEnv, array: JavaByteArray, start: JavaSize, len: JavaSize, buf: *mut JavaByte),
    #[doc(hidden)] pub get_char_array_region: unsafe extern "C" fn(env: *mut JNIEnv, array: JavaCharArray, start: JavaSize, len: JavaSize, buf: *mut JavaChar),
    #[doc(hidden)] pub get_short_array_region: unsafe extern "C" fn(env: *mut JNIEnv, array: JavaShortArray, start: JavaSize, len: JavaSize, buf: *mut JavaShort),
    #[doc(hidden)] pub get_int_array_region: unsafe extern "C" fn(env: *mut JNIEnv, array: JavaIntegerArray, start: JavaSize, len: JavaSize, buf: *mut JavaInteger),
    #[doc(hidden)] pub get_long_array_region: unsafe extern "C" fn(env: *mut JNIEnv, array: JavaLongArray, start: JavaSize, len: JavaSize, buf: *mut JavaLong),
    #[doc(hidden)] pub get_float_array_region: unsafe extern "C" fn(env: *mut JNIEnv, array: JavaFloatArray, start: JavaSize, len: JavaSize, buf: *mut JavaFloat),
    #[doc(hidden)] pub get_double_array_region: unsafe extern "C" fn(env: *mut JNIEnv, array: JavaDoubleArray, start: JavaSize, len: JavaSize, buf: *mut JavaDouble),

    #[doc(hidden)] pub set_boolean_array_region: unsafe extern "C" fn(env: *mut JNIEnv, array: JavaBooleanArray, start: JavaSize, l: JavaSize, buf: *const JavaBoolean),
    #[doc(hidden)] pub set_byte_array_region: unsafe extern "C" fn(env: *mut JNIEnv, array: JavaByteArray, start: JavaSize, len: JavaSize, buf: *const JavaByte),
    #[doc(hidden)] pub set_char_array_region: unsafe extern "C" fn(env: *mut JNIEnv, array: JavaCharArray, start: JavaSize, len: JavaSize, buf: *const JavaChar),
    #[doc(hidden)] pub set_short_array_region: unsafe extern "C" fn(env: *mut JNIEnv, array: JavaShortArray, start: JavaSize, len: JavaSize, buf: *const JavaShort),
    #[doc(hidden)] pub set_int_array_region: unsafe extern "C" fn(env: *mut JNIEnv, array: JavaIntegerArray, start: JavaSize, len: JavaSize, buf: *const JavaInteger),
    #[doc(hidden)] pub set_long_array_region: unsafe extern "C" fn(env: *mut JNIEnv, array: JavaLongArray, start: JavaSize, len: JavaSize, buf: *const JavaLong),
    #[doc(hidden)] pub set_float_array_region: unsafe extern "C" fn(env: *mut JNIEnv, array: JavaFloatArray, start: JavaSize, len: JavaSize, buf: *const JavaFloat),
    #[doc(hidden)] pub set_double_array_region: unsafe extern "C" fn(env: *mut JNIEnv, array: JavaDoubleArray, start: JavaSize, len: JavaSize, buf: *const JavaDouble),

    #[doc(hidden)] pub register_natives: unsafe extern "C" fn(env: *mut JNIEnv, clazz: JavaClass, methods: *const JNINativeMethod, nMethods: JavaInteger) -> JavaInteger,
    #[doc(hidden)] pub unregister_natives: unsafe extern "C" fn(env: *mut JNIEnv, clazz: JavaClass) -> JavaInteger,

    #[doc(hidden)] pub monitor_enter: unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject) -> JavaInteger,
    #[doc(hidden)] pub monitor_exit: unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject) -> JavaInteger,

    #[doc(hidden)] pub get_java_vm: unsafe extern "C" fn(env: *mut JNIEnv, vm: *mut *mut JavaVM) -> JavaInteger,

    #[doc(hidden)] pub get_string_region: unsafe extern "C" fn(env: *mut JNIEnv, str: JavaStringObj, start: JavaSize, len: JavaSize, buf: *mut JavaChar),
    #[doc(hidden)] pub get_string_utf_region: unsafe extern "C" fn(env: *mut JNIEnv, str: JavaStringObj, start: JavaSize, len: JavaSize, buf: *mut ::std::os::raw::c_char),

    #[doc(hidden)] pub get_primitive_array_critical: unsafe extern "C" fn(env: *mut JNIEnv, array: JavaArray, isCopy: *mut JavaBoolean) -> *mut ::std::os::raw::c_void,
    #[doc(hidden)] pub release_primitive_array_critical: unsafe extern "C" fn(env: *mut JNIEnv, array: JavaArray, carray: *mut ::std::os::raw::c_void, mode: JavaInteger),
    #[doc(hidden)] pub get_string_critical: unsafe extern "C" fn(env: *mut JNIEnv, string: JavaStringObj, isCopy: *mut JavaBoolean) -> *const JavaChar,
    #[doc(hidden)] pub release_string_critical: unsafe extern "C" fn(env: *mut JNIEnv, string: JavaStringObj, cstring: *const JavaChar),

    #[doc(hidden)] pub new_weak_global_ref: unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject) -> JavaWeak,
    #[doc(hidden)] pub delete_weak_global_ref: unsafe extern "C" fn(env: *mut JNIEnv, ref_: JavaWeak),

    #[doc(hidden)] pub exception_check: unsafe extern "C" fn(env: *mut JNIEnv) -> JavaBoolean,

    #[doc(hidden)] pub new_direct_byte_buffer: unsafe extern "C" fn(env: *mut JNIEnv, address: *mut ::std::os::raw::c_void, capacity: JavaLong) -> JavaObject,
    #[doc(hidden)] pub get_direct_buffer_address: unsafe extern "C" fn(env: *mut JNIEnv, buf: JavaObject) -> *mut ::std::os::raw::c_void,
    #[doc(hidden)] pub get_direct_buffer_capacity: unsafe extern "C" fn(env: *mut JNIEnv, buf: JavaObject) -> JavaLong,

    #[doc(hidden)] pub get_object_ref_type: unsafe extern "C" fn(env: *mut JNIEnv, obj: JavaObject) -> JavaObjectRefType,
}

impl Default for JNINativeInterface_ {
    fn default() -> Self {
        unsafe { ::core::mem::zeroed() }
    }
}

#[repr(C)]
#[derive(Debug, Clone, Copy, Hash, PartialEq, Eq)]
/// Array List Tag
pub struct __va_list_tag {
    /// offset
    pub gp_offset: ::std::os::raw::c_uint,
    /// offset
    pub fp_offset: ::std::os::raw::c_uint,
    /// overflow arg area
    pub overflow_arg_area: *mut ::std::os::raw::c_void,
    /// reg save area
    pub reg_save_area: *mut ::std::os::raw::c_void,
}

impl Default for __va_list_tag {
    fn default() -> Self {
        unsafe { ::core::mem::zeroed() }
    }
}
