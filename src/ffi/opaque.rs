// Copyright (c) 2017 rsjni developers
//
// Licensed under the Apache License, Version 2.0
// <LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0> or the MIT
// license <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. All files in the project carrying such notice may not be copied,
// modified, or distributed except according to those terms.

//! Java FFI Opaque structs.

#[repr(C)]
#[derive(Debug, Copy, Clone)]
/// The opaque struct for a Java field id.
pub struct _JavaFieldId {
    #[doc(hidden)]
    _unused: [u8; 0],
}

#[repr(C)]
#[derive(Debug, Copy, Clone)]
/// The opaque struct for a Java method id.
pub struct _JavaMethodId {
    #[doc(hidden)]
    _unused: [u8; 0],
}

#[repr(C)]
#[derive(Debug, Copy, Clone)]
/// The opaque struct for a Java object.
pub struct _JavaObject {
    #[doc(hidden)]
    _unused: [u8; 0],
}
