// Copyright (c) 2017 rsjni developers
//
// Licensed under the Apache License, Version 2.0
// <LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0> or the MIT
// license <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. All files in the project carrying such notice may not be copied,
// modified, or distributed except according to those terms.

//! Java Virtual Machine FFI
use error::{Error, ErrorKind, Result};
use ffi::constants::{JNIEnv, JavaVM, JNI_OK};
use ffi::externs;
use ffi::structs::JavaVMOption;
use jni::{Env, Version};
use std::convert::TryFrom;
use std::ffi::CString;
use std::path::{Path, PathBuf};
use std::ptr::{self, Unique};

/// JVM options required for creating and loading a JVM.
#[derive(Clone, Debug, Default, Eq, Getters, Hash, Ord, PartialEq, PartialOrd, Setters)]
pub struct Opts {
    /// The JNI version you wish to request.
    #[set = "pub"]
    version: Version,
    /// The classpath
    #[set = "pub"]
    classpath: Classpath,
    /// The initial heap size, i.e. 256 = -Xms256m
    #[set = "pub"]
    initial_heap_size: usize,
    /// The maximum heap size, i.e. 256 = -Xmx256m
    #[set = "pub"]
    max_heap_size: usize,
    /// Should unrecognized options be ignored?
    #[set = "pub"]
    ignore_unrecognized: bool,
    /// A vector of custom option strings, i.e. -Denv=dev
    #[set = "pub"]
    custom: Vec<String>,
}

/// A wrapper around the FFI options to maintain `CString` lifetimes.
#[derive(Clone, Debug, Default)]
pub struct FFIOpts {
    /// The JNI version you wish to request.
    pub version: Version,
    /// This is required in order to preserve the existence of the heap allocated CString instance, to prevent it from being dropped while a pointer to its
    /// contents is used in the `JavaVMInitArgs` list.
    pub option_strings: Vec<CString>,
    /// The FFI options.
    pub options: Vec<JavaVMOption>,
    /// Should unrecognized options be ignored?
    pub ignore_unrecognized: bool,
}

impl FFIOpts {
    /// Adds an option to the list of FFI options, used when we're constructing the final options list.
    fn add_option(&mut self, option: String) -> Result<()> {
        let cstr = CString::new(option)?;
        self.options.push(JavaVMOption {
            option_string: cstr.as_ptr(),
            extra_info: ptr::null(),
        });

        // Transfer ownership of the CString to the options struct so that it lives for at least as long as the pointer we just created using .as_ptr() above
        self.option_strings.push(cstr);
        Ok(())
    }
}

impl TryFrom<Opts> for FFIOpts {
    type Error = Error;

    fn try_from(opts: Opts) -> Result<Self> {
        let mut ffi_opts: Self = Default::default();

        // Copy over the version and ignore flag.
        ffi_opts.version = opts.version;
        ffi_opts.ignore_unrecognized = opts.ignore_unrecognized;

        // Don't bother specifying heap size configurations if they're equal to 0, as this is the marker value we used
        if opts.initial_heap_size > 0 {
            let option = format!("-Xms{}m", opts.initial_heap_size);
            ffi_opts.add_option(option)?;
        }

        if opts.max_heap_size > 0 {
            let option = format!("-Xmx{}m", opts.max_heap_size);
            ffi_opts.add_option(option)?;
        }

        // Construct the classpath.
        let classpath: String = opts.classpath.into();
        let mut classpath_opt = String::from("-Djava.class.path=");
        classpath_opt.push_str(&classpath);
        ffi_opts.add_option(classpath_opt)?;

        // Add any custom options.
        for option in &opts.custom {
            ffi_opts.add_option(option.clone())?;
        }

        Ok(ffi_opts)
    }
}

/// Platform specific Java classpath separator character.
#[cfg(unix)]
const CLASSPATH_SEP: char = ':';

/// Platform specific Java classpath separator character.
#[cfg(windows)]
const CLASSPATH_SEP: char = ';';

/// A Java classpath used by the JVM for class location.
#[derive(Clone, Debug, Default, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct Classpath {
    /// A list of paths to put on the JVM classpath.
    paths: Vec<PathBuf>,
}

impl Classpath {
    /// Add a path to the classpath.
    ///
    /// This can either be the path to a directory containing compiled .class files, or the direct path to a .jar archive.
    ///
    /// For example, if you're looking for the `Test.class` file in the folder `examples` relative to the current directory, then you should add the path
    /// `examples` to the classpath. If you've added the `Test.class` file to a jar file at `examples/test.jar`, then you should add the path
    /// `examples/test.jar` to the classpath.
    pub fn add<T: AsRef<Path>>(&mut self, path: T) -> &mut Self {
        self.paths.push(path.as_ref().to_owned());
        self
    }
}

impl From<Classpath> for String {
    fn from(classpath: Classpath) -> Self {
        let mut cp_str = Self::new();

        // Iterate over each path
        for path in classpath.paths {
            let converted_path = path.to_string_lossy().into_owned();
            cp_str.push_str(&converted_path);
            cp_str.push(CLASSPATH_SEP);
        }

        cp_str
    }
}

#[cfg(test)]
mod classpath {
    use super::CLASSPATH_SEP;

    mod test {
        use super::CLASSPATH_SEP;
        use jvm::Classpath;
        use std::path::PathBuf;

        #[test]
        fn classpath() {
            let base_path = PathBuf::from("examples");
            let mut classpath: Classpath = Default::default();
            classpath.add(&base_path);
            let jar_path = base_path.join("lib").join("testme.jar");
            classpath.add(&jar_path);

            let cp_str: String = classpath.into();

            let mut expected = String::from("examples");
            expected.push(CLASSPATH_SEP);
            expected.push_str(&format!("{}", jar_path.display()));
            expected.push(CLASSPATH_SEP);

            assert_eq!(cp_str, expected);
        }
    }
}

unsafe impl Sync for Jvm {}

/// Represents a Java Virtual Machine and associated JNI execution environment.
#[derive(Getters)]
pub struct Jvm {
    /// A pointer to the JVM.
    #[doc(hidden)]
    vm: Unique<JavaVM>,
    /// A pointer to the initial JNIEnv struct.
    #[get = "pub"]
    #[doc(hidden)]
    env: Env,
}

impl Default for Jvm {
    fn default() -> Self {
        Self {
            vm: Unique::empty(),
            env: Default::default(),
        }
    }
}

impl Jvm {
    /// Get the inner `JavaVM` pointer.
    #[doc(hidden)]
    pub fn as_ptr(&self) -> *mut JavaVM {
        self.vm.as_ptr()
    }

    /// Create and load a new JVM with the given `Opts`.
    ///
    /// # Notes
    ///
    /// * **Creation of multiple VMs in a single process is not supported.**
    /// * The current thread becomes the main thread.
    /// * Sets the `Env` to the JNI interface pointer of the main thread.
    /// * If `ignore_unrecognized` in `Opts` is true, this will ignore all unrecognized option strings that begin with "-X" or "_".
    /// * If `ignore_unrecognized` in `Opts` is false, this fuction will error as soon as it encounters any unrecognized option strings.
    /// * When the JVM struct is dropped, the underlying JVM will be destroyed.
    ///
    /// # Errors
    ///
    /// * If the `JavaVM` pointer that is manipulated by the FFI call is null, an `ErrorKind::NullPointer` error is generated.
    /// * If the `JNIEnv` pointer that is manipulated by the FFI call is null, an `ErrorKind::NullPointer` error is generated.
    /// * If the return status is not `JNI_OK` an appropriate status error is generated.
    pub fn new(opts: Opts) -> Result<Self> {
        let mut ffi_opts: FFIOpts = TryFrom::try_from(opts)?;
        let mut vm = ptr::null_mut();
        let mut env = ptr::null_mut();
        let mut args = TryFrom::try_from(&mut ffi_opts)?;

        unsafe {
            let status = externs::JNI_CreateJavaVM(&mut vm, &mut env, &mut args);

            if status < 0 {
                Err(status.into())
            } else if env.is_null() || vm.is_null() {
                Err(ErrorKind::NullPointer.into())
            } else if status == TryFrom::try_from(JNI_OK)? {
                if let Some(uniq_vm) = Unique::new(vm) {
                    Ok(Self { vm: uniq_vm, env: Env::new(env)? })
                } else {
                    Err(ErrorKind::CreateJVM.into())
                }
            } else {
                let msg: String = String::from("creating the JVM");
                Err(ErrorKind::Unknown(msg).into())
            }
        }
    }

    /// Attaches the current thread to this JVM.
    ///
    /// # Notes
    ///
    /// * Trying to attach a thread that is already attached is a no-op.
    /// * A native thread cannot be attached simultaneously to two Java VMs.
    /// * When a thread is attached to the VM, the context class loader is the bootstrap loader.
    ///
    /// # Errors
    ///
    /// * If the `JNIEnv` pointer that is manipulated by the FFI call is null, an `ErrorKind::NullPointer` error is generated.
    /// * If the return status is not `JNI_OK` an appropriate status error is generated.
    pub fn attach_current_thread(&self) -> Result<Env> {
        let jvm_ptr = self.vm.as_ptr();
        let mut env_ptr = ptr::null_mut();
        unsafe {
            let status = ((**jvm_ptr).attach_current_thread)(jvm_ptr, &mut env_ptr, ptr::null_mut());

            if status < 0 {
                Err(status.into())
            } else if env_ptr.is_null() {
                Err(ErrorKind::NullPointer.into())
            } else if status == TryFrom::try_from(JNI_OK)? {
                Ok(Env::new(env_ptr as *mut JNIEnv)?)
            } else {
                let msg: String = String::from("the current thread to the JVM");
                Err(ErrorKind::Unknown(msg).into())
            }
        }
    }

    /// Attaches the current thread to this JVM as a daemon thread.
    ///
    /// # Notes
    ///
    /// * Same semantics as `attach_current_thread`, but the newly-created `java.lang.Thread` instance is a daemon.
    /// * If the thread has already been attached via either `attach_current_thread` or `attach_current_thread_as_daemon`, this routine simply sets the value
    /// pointed to by env_ptr to the `JNIEnv` of the current thread. In this case neither `attach_current_thread` nor this routine have any effect on the daemon
    /// status of the thread.
    ///
    /// # Errors
    ///
    /// * If the `JNIEnv` pointer that is manipulated by the FFI call is null, an `ErrorKind::NullPointer` error is generated.
    /// * If the return status is not `JNI_OK` an appropriate status error is generated.
    pub fn attach_current_thread_as_daemon(&self) -> Result<Env> {
        let jvm_ptr = self.vm.as_ptr();
        let mut env_ptr = ptr::null_mut();
        unsafe {
            let status = ((**jvm_ptr).attach_current_thread_as_daemon)(jvm_ptr, &mut env_ptr, ptr::null_mut());

            if status < 0 {
                Err(status.into())
            } else if env_ptr.is_null() {
                Err(ErrorKind::NullPointer.into())
            } else if status == TryFrom::try_from(JNI_OK)? {
                Ok(Env::new(env_ptr as *mut JNIEnv)?)
            } else {
                let msg: String = String::from("attaching the current thread as a daemon to the JVM");
                Err(ErrorKind::Unknown(msg).into())
            }
        }
    }

    /// Detaches the current thread from a Java VM.
    ///
    /// # Notes
    ///
    /// * All Java monitors held by this thread are released.
    /// * All Java threads waiting for this thread to die are notified.
    /// * The main thread can be detached from the VM.
    ///
    /// # Errors
    ///
    /// * If the return status is not `JNI_OK` an appropriate status error is generated.
    pub fn detach_current_thread(&self) -> Result<()> {
        let jvm_ptr = self.vm.as_ptr();
        unsafe {
            let status = ((**jvm_ptr).detach_current_thread)(jvm_ptr);

            if status == TryFrom::try_from(JNI_OK)? {
                Ok(())
            } else {
                Err(status.into())
            }
        }
    }

    /// Try to get an `Env` from this JVM for the requested JNI `Version`.
    ///
    /// # Errors
    ///
    /// * If the `JNIEnv` pointer that is manipulated by the FFI call is null, an `ErrorKind::NullPointer` error is generated.
    /// * If the current thread is not attached to the VM, generates an `ErrorKind::Detached` error.
    /// * If the specified version is not supported, generates an `ErrorKind::Version` error.
    /// * If the return status is not `JNI_OK` an appropriate status error is generated.
    pub fn get_env(&self, version: &Version) -> Result<()> {
        let jvm_ptr = self.vm.as_ptr();
        let mut env_ptr = ptr::null_mut();

        unsafe {
            let status = ((**jvm_ptr).get_env)(jvm_ptr, &mut env_ptr, version.into());

            if status < 0 {
                Err(status.into())
            } else if env_ptr.is_null() {
                Err(ErrorKind::NullPointer.into())
            } else if status == TryFrom::try_from(JNI_OK)? {
                Ok(())
            } else {
                let msg: String = String::from("getting the JNI environment from the JVM");
                Err(ErrorKind::Unknown(msg).into())
            }
        }
    }
}

impl Drop for Jvm {
    fn drop(&mut self) {
        let jvm = self.vm.as_ptr();
        unsafe {
            let _ = ((**jvm).destroy_java_vm)(jvm);
        }
    }
}

#[cfg(test)]
mod test {
    use error::{Error, ErrorKind};
    use jni::Version;
    use jvm::{Classpath, Jvm, Opts};
    use std::env;
    use std::path::PathBuf;

    #[cfg(feature = "nine")]
    fn set_version(opts: &mut Opts) {
        opts.set_version(Version::V9);
    }

    #[cfg(not(feature = "nine"))]
    fn set_version(opts: &mut Opts) {
        opts.set_version(Version::V18);
    }

    #[test]
    fn second_jvm() {
        let manifest = env::var("CARGO_MANIFEST_DIR").unwrap_or(".".to_string());
        let mut path = PathBuf::from(manifest);
        path.push("examples");
        let mut classpath: Classpath = Default::default();
        classpath.add(path);

        let mut opts: Opts = Default::default();
        opts.set_initial_heap_size(256);
        opts.set_max_heap_size(256);
        set_version(&mut opts);
        opts.set_classpath(classpath);

        // Create the Java virtual machine.
        let res = match Jvm::new(opts) {
            Ok(_) => Ok(()),
            Err(e) => match e {
                Error(ErrorKind::Exists, _) => Err(e),
                _ => Ok(()),
            },
        };
        assert!(res.is_err());
    }
}
