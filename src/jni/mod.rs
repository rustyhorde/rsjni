// Copyright (c) 2017 rsjni developers
//
// Licensed under the Apache License, Version 2.0
// <LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0> or the MIT
// license <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. All files in the project carrying such notice may not be copied,
// modified, or distributed except according to those terms.

//! JNI
use class::Class;
use error::{Error, ErrorKind, Result};
use ffi::constants::{JNIEnv, JNI_OK, JNI_TRUE};
use object::Object;
use std::convert::TryFrom;
use std::ffi::CString;
use std::fmt;
use std::hash::{Hash, Hasher};
use std::ptr::{self, Unique};

/// All possible versions of the JNI.
///
/// Not all of these versions may actually be available, and an "unsupported version" error may be triggered upon creating the JVM.
///
/// The integer values of these versions correspond to the FFI version numbers required by the JVM.
#[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub enum Version {
    /// JNI 1.1
    V11 = 0x0001_0001,
    /// JNI 1.2
    V12 = 0x0001_0002,
    /// JNI 1.4
    V14 = 0x0001_0004,
    /// JNI 1.6
    V16 = 0x0001_0006,
    /// JNI 1.8
    V18 = 0x0001_0008,
    /// JNI 9
    #[cfg(feature = "nine")]
    V9 = 0x0009_0000,
    /// JNI 10
    #[cfg(feature = "ten")]
    V10 = 0x000a_0000,
}

#[cfg(feature = "ten")]
impl Default for Version {
    fn default() -> Self {
        Version::V10
    }
}

#[cfg(feature = "nine")]
impl Default for Version {
    fn default() -> Self {
        Version::V9
    }
}

#[cfg(feature = "eight")]
impl Default for Version {
    fn default() -> Self {
        Version::V18
    }
}

impl fmt::Display for Version {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let jni_version_str = match *self {
            Version::V11 => "JNI 1.1",
            Version::V12 => "JNI 1.2",
            Version::V14 => "JNI 1.4",
            Version::V16 => "JNI 1.6",
            Version::V18 => "JNI 1.8",
            #[cfg(feature = "nine")]
            Version::V9 => "JNI 9",
            #[cfg(feature = "ten")]
            Version::V10 => "JNI_10",
        };
        write!(f, "{}", jni_version_str)
    }
}

impl<'a> From<&'a Version> for i32 {
    fn from(version: &Version) -> Self {
        match *version {
            Version::V11 => 0x0001_0001,
            Version::V12 => 0x0001_0002,
            Version::V14 => 0x0001_0004,
            Version::V16 => 0x0001_0006,
            Version::V18 => 0x0001_0008,
            #[cfg(feature = "nine")]
            Version::V9 => 0x0009_0000,
            #[cfg(feature = "ten")]
            Version::V10 => 0x000a_000,
        }
    }
}

impl TryFrom<i32> for Version {
    type Error = Error;

    fn try_from(val: i32) -> Result<Self> {
        match val {
            0x0001_0001 => Ok(Version::V11),
            0x0001_0002 => Ok(Version::V12),
            0x0001_0004 => Ok(Version::V14),
            0x0001_0006 => Ok(Version::V16),
            0x0001_0008 => Ok(Version::V18),
            #[cfg(feature = "nine")]
            0x0009_0000 => Ok(Version::V9),
            #[cfg(feature = "ten")]
            0x000a_0000 => Ok(Version::V10),
            _ => Err(ErrorKind::TryFromI32(val).into()),
        }
    }
}

/// A JNI Environment
#[derive(Clone)]
pub struct Env {
    /// A pointer to the JNIEnv struct.
    #[doc(hidden)]
    env: Unique<JNIEnv>,
}

impl fmt::Debug for Env {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let env = self.env.as_ptr();
        write!(f, "{:?}", env)
    }
}

impl PartialEq for Env {
    fn eq(&self, other: &Self) -> bool {
        let my_ptr = self.env.as_ptr();
        let other_ptr = other.env.as_ptr();
        my_ptr == other_ptr
    }
}

impl Eq for Env {}

impl Hash for Env {
    fn hash<H: Hasher>(&self, state: &mut H) {
        let my_ptr = self.env.as_ptr();
        my_ptr.hash(state);
    }
}

impl Env {
    /// Get the inner `JNIEnv` pointer.
    #[doc(hidden)]
    pub fn as_ptr(&self) -> *mut JNIEnv {
        self.env.as_ptr()
    }

    /// Create a new `Env` from a `JNIEnv` pointer.
    #[doc(hidden)]
    pub fn new(env: *mut JNIEnv) -> Result<Self> {
        Ok(Self {
            env: Unique::new(env).ok_or_else(|| ErrorKind::NotImplemented)?,
        })
    }

    /// Returns the version of the native method interface.
    pub fn get_version(&self) -> Result<Version> {
        let env = self.as_ptr();
        TryFrom::try_from(unsafe { ((**env).get_version)(env) })
    }

    /// Loads a class from a buffer of raw class data. The buffer containing the raw class data is not referenced by the VM after the `define_class` call
    /// returns, and it may be discarded if desired.
    pub fn define_class<'a>(&'a self, _name: &str, _class_loader: Option<&str>, _buf: &[u8]) -> Result<Class<'a>> {
        Err(ErrorKind::NotImplemented.into())
    }

    /// Find a class on the classpath.
    ///
    /// In JDK release 1.1, this function loads a locally-defined class. It searches the directories and zip files specified by the CLASSPATH environment
    /// variable for the class with the specified name.
    ///
    /// Since Java 2 SDK release 1.2, the Java security model allows non-system classes to load and call native methods. `find_class` locates the class loader
    /// associated with the current native method; that is, the class loader of the class that declared the native method. If the native method belongs to a
    /// system class, no class loader will be involved. Otherwise, the proper class loader will be invoked to load and link the named class.
    ///
    /// Since Java 2 SDK release 1.2, when `find_class` is called through the Invocation Interface, there is no current native method or its associated class
    /// loader. In that case, the result of ClassLoader.getSystemClassLoader is used. This is the class loader the virtual machine creates for applications, and
    /// is able to locate classes listed in the java.class.path property.
    ///
    /// The name argument is a fully-qualified class name or an array type signature. For example, the fully-qualified class name for the `java.lang.String`
    /// class is:
    ///
    /// ```java
    /// java/lang/String
    /// ```
    ///
    /// The array type signature of the array class `java.lang.Object[]` is:
    ///
    /// ```java
    /// [Ljava/lang/Object;
    /// ```
    ///
    pub fn find_class<'a>(&'a self, name: &str) -> Result<Class<'a>> {
        let env = self.as_ptr();
        let cstr = CString::new(name)?;
        let raw = unsafe { ((**env).find_class)(env, cstr.as_ptr()) };

        if raw.is_null() {
            Err(ErrorKind::ClassNotFound(name.to_string()).into())
        } else {
            Ok(Class::new(self, raw))
        }
    }

    /// Determines if an exception is being thrown. The exception stays being thrown until either the native code calls `exception_clear`, or the Java code
    /// handles the exception.
    pub fn exception_ocurred(&self) -> Result<Object> {
        let env = self.as_ptr();
        let obj_ptr = unsafe { ((**env).exception_ocurred)(env) };

        if obj_ptr.is_null() {
            Err(ErrorKind::NullPointer.into())
        } else {
            Ok(Object::new(self, obj_ptr))
        }
    }

    /// Prints an exception and a backtrace of the stack to a system error-reporting channel, such as stderr. The pending exception is cleared as a side-effect
    /// of calling this function. This is a convenience routine provided for debugging.
    pub fn exception_describe(&self) {
        let env = self.as_ptr();
        unsafe { ((**env).exception_describe)(env) }
    }

    /// Clears any exception that is currently being thrown. If no exception is currently being thrown, this routine has no effect.
    pub fn exception_clear(&self) {
        let env = self.as_ptr();
        unsafe { ((**env).exception_clear)(env) };
    }

    /// Raises a fatal error and does not expect the VM to recover. This function does not return.
    pub fn fatal_error(&self, msg: &str) -> Result<()> {
        let env = self.as_ptr();
        let msg_str = CString::new(msg)?;
        unsafe {
            ((**env).fatal_error)(env, msg_str.as_ptr());
        }
        Ok(())
    }

    /// A convenience function to check for pending exceptions without creating a local reference to the exception object.
    pub fn exception_check(&self) -> Result<bool> {
        let env = self.as_ptr();
        unsafe { Ok(((**env).exception_check)(env) == u8::try_from(JNI_TRUE)?) }
    }

    /// Creates a new global reference to the object referred to by the obj argument. The obj argument may be a global or local reference. Global references
    /// must be explicitly disposed of by calling `delete_global_ref`.
    pub fn new_global_ref<'a>(&'a self, obj: &Object) -> Result<Object<'a>> {
        let env = self.as_ptr();
        unsafe {
            let global_ref = ((**env).new_global_ref)(env, *(*obj).raw());

            if global_ref.is_null() {
                Err(ErrorKind::NullPointer.into())
            } else {
                Ok(Object::new(self, global_ref))
            }
        }
    }

    /// Deletes the global reference pointed to by the given object.
    pub fn delete_global_ref(&self, obj: &Object) {
        let env = self.as_ptr();
        unsafe { ((**env).delete_global_ref)(env, *obj.raw()) }
    }

    /// Deletes the local reference pointed to by the given object.
    ///
    /// # Notes
    ///
    /// * JDK/JRE 1.1 provides the `delete_local_ref` function so that programmers can manually delete local references. For example, if native code iterates
    /// through a potentially large array of objects and uses one element in each iteration, it is a good practice to delete the local reference to the
    /// no-longer-used array element before a new local reference is created in the next iteration. As of JDK/JRE 1.2 an additional set of functions are
    /// provided for local reference lifetime management. They are the four functions below.
    pub fn delete_local_ref(&self, obj: &Object) {
        let env = self.as_ptr();
        unsafe { ((**env).delete_local_ref)(env, *obj.raw()) }
    }

    /// Ensures that at least a given number of local references can be created in the current thread.
    ///
    /// # Notes
    ///
    /// * Before it enters a native method, the VM automatically ensures that at least 16 local references can be created.
    /// * For backward compatibility, the VM allocates local references beyond the ensured capacity. (As a debugging support, the VM may give the user warnings
    /// that too many local references are being created. In the JDK, the programmer can supply the -verbose:jni command line option to turn on these messages.)
    /// The VM calls FatalError if no more local references can be created beyond the ensured capacity.
    /// * Some Java Virtual Machine implementations may choose to limit the maximum capacity, which may cause the function to return an error (e.g. JNI_ERR or
    /// JNI_EINVAL). The HotSpot JVM implementation, for example, uses the -XX:+MaxJNILocalCapacity flag (default: 65536).
    pub fn ensure_local_capacity(&self, size: u32) -> Result<()> {
        let env = self.as_ptr();

        unsafe {
            let status = ((**env).ensure_local_capacity)(env, TryFrom::try_from(size)?);

            if status < 0 {
                Err(status.into())
            } else if status == TryFrom::try_from(JNI_OK)? {
                Ok(())
            } else {
                let msg: String = String::from("ensuring the local capacity");
                Err(ErrorKind::Unknown(msg).into())
            }
        }
    }

    /// Creates a new local reference frame, in which at least a given number of local references can be created.
    ///
    /// # Notes
    ///
    /// * Local references already created in previous local frames are still valid in the current local frame.
    /// * As with `ensure_local_capacity`, some Java Virtual Machine implementations may choose to limit the maximum capacity, which may cause the function to
    /// return an error.
    pub fn push_local_frame(&self, size: u32) -> Result<()> {
        let env = self.as_ptr();

        unsafe {
            let status = ((**env).push_local_frame)(env, TryFrom::try_from(size)?);

            if status < 0 {
                Err(status.into())
            } else if status == TryFrom::try_from(JNI_OK)? {
                Ok(())
            } else {
                let msg: String = String::from("ensuring the local capacity");
                Err(ErrorKind::Unknown(msg).into())
            }
        }
    }

    /// Pops off the current local reference frame, frees all the local references, and returns a local reference in the previous local reference frame for the
    /// given result object.
    ///
    /// # Notes
    ///
    /// * Pass `None` as result if you do not need to return a reference to the previous frame.
    pub fn pop_local_frame(&self, result: &Option<Object>) -> Result<Option<Object>> {
        let env = self.as_ptr();
        let res_ptr = if let Some(ref res) = *result { *(*res).raw() } else { ptr::null_mut() };

        unsafe {
            let obj_ref = ((**env).pop_local_frame)(env, res_ptr);

            if obj_ref.is_null() && result.is_some() {
                Err(ErrorKind::NullPointer.into())
            } else if obj_ref.is_null() && result.is_none() {
                Ok(None)
            } else {
                Ok(Some(Object::new(self, obj_ref)))
            }
        }
    }

    /// Creates a new local reference that refers to the given object. The given ref may be a global or a local reference.
    pub fn new_local_ref(&self, obj: &Object) -> Result<Object> {
        let env = self.as_ptr();
        unsafe {
            let obj_ref = ((**env).new_local_ref)(env, *(*obj).raw());

            if obj_ref.is_null() {
                Err(ErrorKind::NullPointer.into())
            } else {
                Ok(Object::new(self, obj_ref))
            }
        }
    }
}

impl Default for Env {
    fn default() -> Self {
        Self { env: Unique::empty() }
    }
}

#[cfg(test)]
mod test {
    use error::{Error, ErrorKind, Result};
    use jni::{Env, Version};
    use params::{Input, Kind};
    use test_jvm::att_det;

    #[cfg(feature = "ten")]
    fn check_jni_version(env: &Env) -> Result<()> {
        assert_eq!(Version::V10, env.get_version()?);
        Ok(())
    }

    #[cfg(feature = "nine")]
    fn check_jni_version(env: &Env) -> Result<()> {
        assert_eq!(Version::V9, env.get_version()?);
        Ok(())
    }

    #[cfg(feature = "eight")]
    fn check_jni_version(env: &Env) -> Result<()> {
        assert_eq!(Version::V18, env.get_version()?);
        Ok(())
    }

    #[test]
    fn check_version() {
        assert!(att_det(|env| check_jni_version(env)).is_ok());
    }

    #[test]
    fn find_class() {
        assert!(att_det(|env| {
            env.find_class("Test")?;
            Ok(())
        })
        .is_ok());
    }

    #[test]
    fn class_not_found() {
        assert!(att_det(|env| match env.find_class("IDontExist") {
            Ok(_) => Ok(()),
            Err(e) => match e {
                Error(ErrorKind::ClassNotFound(_), _) => {
                    assert!(env.exception_check()?);
                    env.exception_clear();
                    Err(e)
                }
                _ => Ok(()),
            },
        },)
        .is_err());
    }

    #[test]
    fn global_ref() {
        assert!(att_det(|env| {
            let class = env.find_class("Test")?;
            let constructor_id = class.get_method_id("<init>", &[Kind::Int], &Kind::Void)?;
            let obj = class.new_object(&constructor_id, &[Input::Int(10)])?;
            let gr = env.new_global_ref(&obj)?;
            env.delete_global_ref(&gr);
            Ok(())
        })
        .is_ok());
    }

    #[test]
    fn ensure_local_capacity() {
        assert!(att_det(|env| {
            assert!(env.ensure_local_capacity(100).is_ok());
            Ok(())
        })
        .is_ok());
    }

    #[test]
    fn not_enough_capacity() {
        assert!(att_det(|env| {
            assert!(env.ensure_local_capacity(100_000).is_err());
            Ok(())
        })
        .is_ok());
    }

    #[test]
    fn push_pop_local_frame_with_res() {
        assert!(att_det(|env| {
            env.push_local_frame(100)?;
            let class = env.find_class("Test")?;
            let constructor_id = class.get_method_id("<init>", &[Kind::Int], &Kind::Void)?;
            let obj = class.new_object(&constructor_id, &[Input::Int(100)])?;
            if let Some(res_obj) = env.pop_local_frame(&Some(obj))? {
                let inc_method_id = class.get_method_id("incrementCurrent", &[], &Kind::Void)?;
                res_obj.call_method(&inc_method_id, &[])?;
                assert!(true);
            } else {
                assert!(false);
            }
            Ok(())
        })
        .is_ok());
    }

    #[test]
    fn push_pop_local_frame_without_res() {
        assert!(att_det(|env| {
            env.push_local_frame(100)?;
            assert!(env.pop_local_frame(&None)?.is_none());
            Ok(())
        })
        .is_ok());
    }

    #[test]
    fn new_local_ref() {
        assert!(att_det(|env| {
            let class = env.find_class("Test")?;
            let constructor_id = class.get_method_id("<init>", &[Kind::Int], &Kind::Void)?;
            let obj = class.new_object(&constructor_id, &[Input::Int(10)])?;
            let local_ref = env.new_local_ref(&obj)?;
            let inc_method_id = class.get_method_id("incrementCurrent", &[], &Kind::Void)?;
            local_ref.call_method(&inc_method_id, &[])?;
            Ok(())
        })
        .is_ok());
    }
}
