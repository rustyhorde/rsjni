// Copyright (c) 2017 rsjni developers
//
// Licensed under the Apache License, Version 2.0
// <LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0> or the MIT
// license <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. All files in the project carrying such notice may not be copied,
// modified, or distributed except according to those terms.

//! Java NIO byte buffer convenience wrapper.
use error::Result;
use jni::Env;
use object::Object;
use std::os::raw::c_void;
#[cfg(target_pointer_width = "64")]
use utils::PrivateTryFromUsize;

/// Convience wrapper around a regular Java object for a Java NIO byte buffer.
#[derive(Clone, Getters)]
pub struct ByteBuffer<'a> {
    #[doc(hidden)]
    /// The Env that created this object.
    env: &'a Env,
    #[doc(hidden)]
    /// The Java object.
    #[get = "pub"]
    inner: Object<'a>,
    #[doc(hidden)]
    /// The underlying byte buffer.
    buf: &'a [u8],
    #[doc(hidden)]
    /// The length of the buffer.
    len: u32,
}

impl<'a> ByteBuffer<'a> {
    /// Create a new `ByteBuffer` wrapping the given mutable byte array.
    ///
    /// # Notes
    ///
    /// * Allocates and returns a direct `java.nio.ByteBuffer` referring to the block of memory starting at the memory address of the given buf.
    /// * Native code that calls this function and returns the resulting byte-buffer object to Java-level code should ensure that the buffer refers to a valid
    /// region of memory that is accessible for reading and, if appropriate, writing. An attempt to access an invalid memory location from Java code will either
    /// return an arbitrary value, have no visible effect, or cause an unspecified exception to be thrown.
    #[cfg(target_pointer_width = "32")]
    pub fn new(env: &'a Env, buf: &'a mut [u8]) -> Result<Self> {
        let env_ptr = env.as_ptr();
        let buf_ptr = buf.as_mut_ptr() as *mut c_void;
        let len = buf.len();
        unsafe {
            let byte_buf_ptr = ((**env_ptr).new_direct_byte_buffer)(env_ptr, buf_ptr, len as i64);

            Ok(Self {
                env,
                inner: Object::new(env, byte_buf_ptr),
                buf,
                len: len as u32,
            })
        }
    }

    /// Create a new `ByteBuffer` wrapping the given mutable byte array.
    ///
    /// # Notes
    ///
    /// * Allocates and returns a direct `java.nio.ByteBuffer` referring to the block of memory starting at the memory address of the given buf.
    /// * Native code that calls this function and returns the resulting byte-buffer object to Java-level code should ensure that the buffer refers to a valid
    /// region of memory that is accessible for reading and, if appropriate, writing. An attempt to access an invalid memory location from Java code will either
    /// return an arbitrary value, have no visible effect, or cause an unspecified exception to be thrown.
    #[cfg(target_pointer_width = "64")]
    pub fn new(env: &'a Env, buf: &'a mut [u8]) -> Result<Self> {
        let env_ptr = env.as_ptr();
        let buf_ptr = buf.as_mut_ptr() as *mut c_void;
        let len = buf.len();
        unsafe {
            let byte_buf_ptr = ((**env_ptr).new_direct_byte_buffer)(env_ptr, buf_ptr, len as i64);

            Ok(Self {
                env,
                inner: Object::new(env, byte_buf_ptr),
                buf,
                len: u32::private_try_from(len)?,
            })
        }
    }
}
