// Copyright (c) 2017 rsjni developers
//
// Licensed under the Apache License, Version 2.0
// <LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0> or the MIT
// license <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. All files in the project carrying such notice may not be copied,
// modified, or distributed except according to those terms.

//! java/lang/String wrapper for convenience.
use error::{Error, Result};
use jni::Env;
use object::Object;
use std::convert::TryFrom;
use std::ffi::{CStr, CString};
use std::os::raw::c_char;
use std::ptr;

/// Convience wrapper around a regular Java object for java/lang/String.
#[derive(Clone, Getters)]
pub struct JavaString<'a> {
    /// The Java object.
    #[doc(hidden)]
    #[get = "pub"]
    inner: Object<'a>,
    /// A pointer to the array in the JVM.  We hold this until Drop so it isn't GCd.
    #[doc(hidden)]
    ptr: *const c_char,
}

impl<'a> JavaString<'a> {
    /// Create a new UTF java/lang/String in the JVM with the give value.
    pub fn new(env: &'a Env, val: &str) -> Result<Self> {
        let env_ptr = env.as_ptr();
        let cstr = CString::new(val)?;
        let java_str = unsafe { ((**env_ptr).new_string_utf)(env_ptr, cstr.as_ptr()) };
        let str_ptr = unsafe { ((**env_ptr).get_string_utf_chars)(env_ptr, java_str, ptr::null_mut()) };
        Ok(Self {
            inner: Object::new(env, java_str),
            ptr: str_ptr,
        })
    }
}

impl<'a> TryFrom<Object<'a>> for JavaString<'a> {
    type Error = Error;

    fn try_from(obj: Object<'a>) -> Result<Self> {
        let env = obj.env.as_ptr();
        let ptr = unsafe { ((**env).get_string_utf_chars)(env, *obj.raw(), ptr::null_mut()) };

        Ok(Self { inner: obj, ptr })
    }
}

impl<'a> TryFrom<JavaString<'a>> for String {
    type Error = Error;

    fn try_from(java_str: JavaString<'a>) -> Result<Self> {
        let mut result = Self::new();
        // Convert the JNI string to something we can actually use
        let name = unsafe { CStr::from_ptr(java_str.ptr) };

        // Copy the UTF-8 converted string into a new, heap allocated one
        let utf8 = name.to_str()?;
        result.push_str(utf8);
        Ok(result)
    }
}

impl<'a> Drop for JavaString<'a> {
    fn drop(&mut self) {
        let env = self.inner.env.as_ptr();

        unsafe { ((**env).release_string_utf_chars)(env, self.inner.raw, self.ptr) };
    }
}
