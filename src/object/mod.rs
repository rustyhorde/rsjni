// Copyright (c) 2017 rsjni developers
//
// Licensed under the Apache License, Version 2.0
// <LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0> or the MIT
// license <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. All files in the project carrying such notice may not be copied,
// modified, or distributed except according to those terms.

//! Java Object Wrapper
use class::{Class, FieldId, MethodId};
use error::{ErrorKind, Result};
use ffi::constants::{JavaObject, JNI_FALSE, JNI_TRUE};
use ffi::enums::JavaObjectRefType;
use ffi::union::JavaValue;
use jni::Env;
use object::strng::JavaString;
use params::{Input, Kind, Output};
use std::convert::TryFrom;

pub mod array;
pub mod nio;
pub mod strng;

/// Java Object reference type
#[derive(Clone, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub enum RefType {
    /// Invalid Reference
    Invalid,
    /// Local Reference
    Local,
    /// Global Reference
    Global,
    /// Weak Global Reference
    WeakGlobal,
}

impl From<JavaObjectRefType> for RefType {
    fn from(ref_type: JavaObjectRefType) -> Self {
        match ref_type {
            JavaObjectRefType::Invalid => RefType::Invalid,
            JavaObjectRefType::Local => RefType::Local,
            JavaObjectRefType::Global => RefType::Global,
            JavaObjectRefType::WeakGlobal => RefType::WeakGlobal,
        }
    }
}

/// Represents a Java object (an instance of a Java class).
///
/// Public method and field access occurs through the object.
///
/// The object has a prescribed lifetime, since it cannot outlive the `Env` that created it.
#[derive(Clone, Getters)]
pub struct Object<'a> {
    /// The Env that created this object.
    env: &'a Env,
    /// The raw object pointer.
    #[doc(hidden)]
    #[get = "pub"]
    raw: JavaObject,
}

impl<'a> Object<'a> {
    /// Create a new `Object`.  This will create a global reference so the object isn't GC'd behind us.
    /// On drop the reference will be deleted so the object can be GCd.
    pub fn new(env: &'a Env, raw: JavaObject) -> Self {
        let env_ptr = env.as_ptr();
        let raw_global = unsafe { ((**env_ptr).new_global_ref)(env_ptr, raw) };
        Self { env, raw: raw_global }
    }

    /// Causes this object to be thrown if it is assignable from 'java.lang.Throwable'.
    pub fn throw(&self) -> Result<()> {
        let env_ptr = self.env.as_ptr();
        let my_class = self.get_object_class();
        let throwable_class = self.env.find_class("java/lang/Throwable")?;

        if my_class.is_assignable_from(&throwable_class) {
            unsafe {
                let status = ((**env_ptr).throw)(env_ptr, self.raw);

                if status < 0 {
                    Err(status.into())
                } else {
                    Ok(())
                }
            }
        } else {
            Err(ErrorKind::NotAssignableFromThrowable.into())
        }
    }

    /// Returns the class of this object.
    pub fn get_object_class(&self) -> Class<'a> {
        let env = self.env.as_ptr();
        Class::new(self.env, unsafe { ((**env).get_object_class)(env, self.raw) })
    }

    /// Returns the reference type of this object.
    pub fn get_object_ref_type(&self) -> Result<RefType> {
        let env = self.env.as_ptr();

        unsafe {
            let ref_type = ((**env).get_object_ref_type)(env, self.raw);
            Ok(ref_type.into())
        }
    }

    /// Tests whether an object is an instance of a class.
    pub fn is_instance_of(&self, class: &Class) -> Result<bool> {
        let env = self.env.as_ptr();

        unsafe {
            let status = ((**env).is_instance_of)(env, self.raw, *(*class).raw());

            if status == u8::try_from(JNI_TRUE)? {
                Ok(true)
            } else if status == u8::try_from(JNI_FALSE)? {
                Ok(false)
            } else {
                let msg: String = String::from("determing instance of");
                Err(ErrorKind::Unknown(msg).into())
            }
        }
    }

    /// Tests whether two references refer to the same Java object.
    pub fn is_same_object(&self, obj: &Object) -> Result<bool> {
        let env = self.env.as_ptr();

        unsafe {
            let status = ((**env).is_same_object)(env, self.raw, *obj.raw());

            if status == u8::try_from(JNI_TRUE)? {
                Ok(true)
            } else if status == u8::try_from(JNI_FALSE)? {
                Ok(false)
            } else {
                let msg: String = String::from("determing instance of");
                Err(ErrorKind::Unknown(msg).into())
            }
        }
    }

    /// Get the value of an instance (nonstatic) field of an object.
    pub fn get_field(&self, field_id: &FieldId) -> Result<Output> {
        let env = self.env.as_ptr();
        let field_id_ptr = *(*field_id).raw();
        let kind = field_id.kind();

        let result = unsafe {
            match *kind {
                Kind::Boolean => Some(JavaValue {
                    boolean: ((**env).get_boolean_field)(env, self.raw, field_id_ptr),
                }),
                Kind::Byte => Some(JavaValue {
                    byte: ((**env).get_byte_field)(env, self.raw, field_id_ptr),
                }),
                Kind::Char => Some(JavaValue {
                    character: ((**env).get_char_field)(env, self.raw, field_id_ptr),
                }),
                Kind::Short => Some(JavaValue {
                    short: ((**env).get_short_field)(env, self.raw, field_id_ptr),
                }),
                Kind::Int => Some(JavaValue {
                    integer: ((**env).get_int_field)(env, self.raw, field_id_ptr),
                }),
                Kind::Long => Some(JavaValue {
                    long: ((**env).get_long_field)(env, self.raw, field_id_ptr),
                }),
                Kind::Float => Some(JavaValue {
                    float: ((**env).get_float_field)(env, self.raw, field_id_ptr),
                }),
                Kind::Double => Some(JavaValue {
                    double: ((**env).get_double_field)(env, self.raw, field_id_ptr),
                }),
                Kind::Object(_) | Kind::BooleanArr | Kind::ByteArr | Kind::CharArr | Kind::ShortArr | Kind::IntArr | Kind::LongArr | Kind::FloatArr | Kind::DoubleArr | Kind::ObjectArr(_) => {
                    Some(JavaValue {
                        object: ((**env).get_object_field)(env, self.raw, field_id_ptr),
                    })
                }
                Kind::Void => unreachable!(),
            }
        };

        if self.env.exception_check()? {
            Err(ErrorKind::NotImplemented.into())
        } else {
            Ok(Output::try_from(result, kind, self.env)?)
        }
    }

    /// Sets the value of an instance (nonstatic) field of an object.
    pub fn set_field(&self, field_id: &FieldId, value: &Input) -> Result<()> {
        let env = self.env.as_ptr();
        let field_id_ptr = *(*field_id).raw();

        // Convert the value into a useable form
        let java_value: JavaValue = TryFrom::try_from(value)?;

        unsafe {
            match *value {
                Input::Boolean(_) => ((**env).set_boolean_field)(env, self.raw, field_id_ptr, java_value.boolean),
                Input::Byte(_) => ((**env).set_byte_field)(env, self.raw, field_id_ptr, java_value.byte),
                Input::Char(_) => ((**env).set_char_field)(env, self.raw, field_id_ptr, java_value.character),
                Input::Short(_) => ((**env).set_short_field)(env, self.raw, field_id_ptr, java_value.short),
                Input::Int(_) => ((**env).set_int_field)(env, self.raw, field_id_ptr, java_value.integer),
                Input::Long(_) => ((**env).set_long_field)(env, self.raw, field_id_ptr, java_value.long),
                Input::Float(_) => ((**env).set_float_field)(env, self.raw, field_id_ptr, java_value.float),
                Input::Double(_) => ((**env).set_double_field)(env, self.raw, field_id_ptr, java_value.double),
                Input::Object(_)
                | Input::BooleanArr(_)
                | Input::ByteArr(_)
                | Input::CharArr(_)
                | Input::ShortArr(_)
                | Input::IntArr(_)
                | Input::LongArr(_)
                | Input::FloatArr(_)
                | Input::DoubleArr(_)
                | Input::ObjectArr(_)
                | Input::StringObj(_) => ((**env).set_object_field)(env, self.raw, field_id_ptr, java_value.object),
            }
        }

        // Convert the result into a value
        if self.env.exception_check()? {
            Err(ErrorKind::NotImplemented.into())
        } else {
            Ok(())
        }
    }

    /// This operation is used to call a Java instance method from a native method.
    ///
    /// # Notes
    ///
    /// * These families of operations invoke an instance (nonstatic) method on a Java object, according to the specified method ID.
    /// * The methodID argument must be obtained by calling `get_method_id`.
    /// * When these functions are used to call private methods and constructors, the method ID must be derived from the real class of obj, not from one of its
    /// superclasses.
    ///
    pub fn call_method(&self, method_id: &MethodId, args: &[Input]) -> Result<Output> {
        let env = self.env.as_ptr();
        let method_id_ptr = *(*method_id).raw();
        let kind = (*method_id).kind();

        let mut java_args = Vec::with_capacity(args.len());
        for arg in args {
            let java_value: JavaValue = TryFrom::try_from(arg)?;
            java_args.push(java_value);
        }

        let result = unsafe {
            match *kind {
                Kind::Boolean => Some(JavaValue {
                    boolean: ((**env).call_boolean_method_a)(env, self.raw, method_id_ptr, java_args.as_ptr()),
                }),
                Kind::Byte => Some(JavaValue {
                    byte: ((**env).call_byte_method_a)(env, self.raw, method_id_ptr, java_args.as_ptr()),
                }),
                Kind::Char => Some(JavaValue {
                    character: ((**env).call_char_method_a)(env, self.raw, method_id_ptr, java_args.as_ptr()),
                }),
                Kind::Short => Some(JavaValue {
                    short: ((**env).call_short_method_a)(env, self.raw, method_id_ptr, java_args.as_ptr()),
                }),
                Kind::Int => Some(JavaValue {
                    integer: ((**env).call_int_method_a)(env, self.raw, method_id_ptr, java_args.as_ptr()),
                }),
                Kind::Long => Some(JavaValue {
                    long: ((**env).call_long_method_a)(env, self.raw, method_id_ptr, java_args.as_ptr()),
                }),
                Kind::Float => Some(JavaValue {
                    float: ((**env).call_float_method_a)(env, self.raw, method_id_ptr, java_args.as_ptr()),
                }),
                Kind::Double => Some(JavaValue {
                    double: ((**env).call_double_method_a)(env, self.raw, method_id_ptr, java_args.as_ptr()),
                }),
                Kind::Object(_) | Kind::BooleanArr | Kind::ByteArr | Kind::CharArr | Kind::ShortArr | Kind::IntArr | Kind::LongArr | Kind::FloatArr | Kind::DoubleArr | Kind::ObjectArr(_) => {
                    Some(JavaValue {
                        object: ((**env).call_object_method_a)(env, self.raw, method_id_ptr, java_args.as_ptr()),
                    })
                }
                Kind::Void => {
                    ((**env).call_void_method_a)(env, self.raw, method_id_ptr, java_args.as_ptr());
                    None
                }
            }
        };

        if self.env.exception_check()? {
            Err(ErrorKind::NotImplemented.into())
        } else {
            Ok(Output::try_from(result, kind, self.env)?)
        }
    }

    /// This operation invokes an instance (nonstatic) method on a Java object, according to the specified class and method ID. The methodID argument must be
    /// obtained by calling `get_method_id` on the given class.
    ///
    /// # Notes
    ///
    /// * The `call_nonvirtual_method` routine and the `call_method` routine are different. The `call_method` routine invokes the method based on the class or
    /// interface of the object, while the `call_nonvirtual_method` routine invokes the method based on the class, designated by the class parameter, from which
    /// the method ID is obtained. The method ID must be obtained from the real class of the object or from one of its supertypes.
    /// * `call_nonvirtual_method` is the mechanism for invoking "default interface methods" introduced in Java 8.
    pub fn call_nonvirtual_method(&self, class: &Class, method_id: &MethodId, args: &[Input]) -> Result<Output> {
        let env = self.env.as_ptr();
        let class_ptr = *(*class).raw();
        let method_id_ptr = *(*method_id).raw();
        let kind = (*method_id).kind();

        let mut java_args = Vec::with_capacity(args.len());
        for arg in args {
            let java_value: JavaValue = TryFrom::try_from(arg)?;
            java_args.push(java_value);
        }

        let result = unsafe {
            match *kind {
                Kind::Boolean => Some(JavaValue {
                    boolean: ((**env).call_nonvirtual_boolean_method_a)(env, self.raw, class_ptr, method_id_ptr, java_args.as_ptr()),
                }),
                Kind::Byte => Some(JavaValue {
                    byte: ((**env).call_nonvirtual_byte_method_a)(env, self.raw, class_ptr, method_id_ptr, java_args.as_ptr()),
                }),
                Kind::Char => Some(JavaValue {
                    character: ((**env).call_nonvirtual_char_method_a)(env, self.raw, class_ptr, method_id_ptr, java_args.as_ptr()),
                }),
                Kind::Short => Some(JavaValue {
                    short: ((**env).call_nonvirtual_short_method_a)(env, self.raw, class_ptr, method_id_ptr, java_args.as_ptr()),
                }),
                Kind::Int => Some(JavaValue {
                    integer: ((**env).call_nonvirtual_int_method_a)(env, self.raw, class_ptr, method_id_ptr, java_args.as_ptr()),
                }),
                Kind::Long => Some(JavaValue {
                    long: ((**env).call_nonvirtual_long_method_a)(env, self.raw, class_ptr, method_id_ptr, java_args.as_ptr()),
                }),
                Kind::Float => Some(JavaValue {
                    float: ((**env).call_nonvirtual_float_method_a)(env, self.raw, class_ptr, method_id_ptr, java_args.as_ptr()),
                }),
                Kind::Double => Some(JavaValue {
                    double: ((**env).call_nonvirtual_double_method_a)(env, self.raw, class_ptr, method_id_ptr, java_args.as_ptr()),
                }),
                Kind::Object(_) | Kind::BooleanArr | Kind::ByteArr | Kind::CharArr | Kind::ShortArr | Kind::IntArr | Kind::LongArr | Kind::FloatArr | Kind::DoubleArr | Kind::ObjectArr(_) => {
                    Some(JavaValue {
                        object: ((**env).call_nonvirtual_object_method_a)(env, self.raw, class_ptr, method_id_ptr, java_args.as_ptr()),
                    })
                }
                Kind::Void => {
                    ((**env).call_nonvirtual_void_method_a)(env, self.raw, class_ptr, method_id_ptr, java_args.as_ptr());
                    None
                }
            }
        };

        if self.env.exception_check()? {
            Err(ErrorKind::NotImplemented.into())
        } else {
            Ok(Output::try_from(result, kind, self.env)?)
        }
    }

    /// Enters the monitor associated with this object.
    ///
    /// # Notes
    ///
    /// * Each Java object has a monitor associated with it. If the current thread already owns the monitor associated with the object, it increments a counter
    /// in the monitor indicating the number of times this thread has entered the monitor. If the monitor associated with the object is not owned by any thread,
    /// the current thread becomes the owner of the monitor, setting the entry count of this monitor to 1. If another thread already owns the monitor associated
    /// with the object, the current thread waits until the monitor is released, then tries again to gain ownership.
    /// * A monitor entered through a MonitorEnter JNI function call cannot be exited using the monitorexit Java virtual machine instruction or a synchronized
    /// method return.
    /// * A MonitorEnter JNI function call and a monitorenter Java virtual machine instruction may race to enter the monitor associated with the
    /// same object.
    /// * To avoid deadlocks, a monitor entered through a MonitorEnter JNI function call must be exited using the MonitorExit JNI call, unless the
    /// `detach_current_thread` call is used to implicitly release JNI monitors.
    pub fn monitor_enter(&self) -> Result<()> {
        let env = self.env.as_ptr();
        let status = unsafe { ((**env).monitor_enter)(env, self.raw) };

        if status < 0 {
            Err(status.into())
        } else {
            Ok(())
        }
    }

    /// The current thread must be the owner of the monitor associated with the underlying Java object referred to by this object. The thread decrements the
    /// counter indicating the number of times it has entered this monitor. If the value of the counter becomes zero, the current thread releases the monitor.
    ///
    /// # Notes
    ///
    /// * Native code must not use MonitorExit to exit a monitor entered through a synchronized method or a monitorenter Java virtual machine instruction.
    pub fn monitor_exit(&self) -> Result<()> {
        let env = self.env.as_ptr();
        let status = unsafe { ((**env).monitor_exit)(env, self.raw) };

        if status < 0 {
            Err(status.into())
        } else {
            Ok(())
        }
    }

    /// Returns a mutable buffer that allows direct access to the buffer behind this `java.nio.ByteBuffer`.
    ///
    /// # Notes
    ///
    /// * This function allows native code to access the same memory region that is accessible to Java code via the buffer object.
    pub fn get_direct_buffer(&self) -> Result<&mut [u8]> {
        let env = self.env.as_ptr();

        // TODO: check that this object is actually a java.nio.ByteBuffer.
        unsafe {
            let len = ((**env).get_direct_buffer_capacity)(env, self.raw);
            let addr = ((**env).get_direct_buffer_address)(env, self.raw);
            let addr_ptr = addr as *mut u8;
            let buf = ::std::slice::from_raw_parts_mut(addr_ptr, TryFrom::try_from(len)?);
            Ok(buf)
        }
    }

    /// Converts a `java.lang.reflect.Field` to a field ID.
    pub fn from_reflected_field(&self) -> Result<FieldId> {
        let env = self.env.as_ptr();
        let field_class = self.env.find_class("java/lang/reflect/Field")?;

        if self.is_instance_of(&field_class)? {
            // Getting the canonical name here so we can figure out our `Kind`.
            let my_class = self.get_object_class();
            let get_type_id = my_class.get_method_id("getType", &[], &Kind::Object("java/lang/Class".to_string()))?;
            let get_type_output = self.call_method(&get_type_id, &[])?;
            let class_obj = get_type_output.object();
            let class_class = class_obj.get_object_class();
            let name_id = class_class.get_method_id("getName", &[], &Kind::Object("java/lang/String".to_string()))?;
            let name_output = class_obj.call_method(&name_id, &[])?;
            let name_str: strng::JavaString = TryFrom::try_from(name_output.object())?;
            let name: String = TryFrom::try_from(name_str)?;
            let my_kind: Kind = name.into();

            unsafe {
                let field_id = ((**env).from_reflected_field)(env, self.raw);
                Ok(FieldId::new(self.env, field_id, my_kind))
            }
        } else {
            Err("This is not a 'java.lang.reflect.Field' object!".into())
        }
    }

    ///
    pub fn from_reflected_method(&self) -> Result<MethodId> {
        let env = self.env.as_ptr();
        let method_class = self.env.find_class("java/lang/reflect/Method")?;

        if self.is_instance_of(&method_class)? {
            // Getting the canonical name here so we can figure out our `Kind`.
            let my_class = self.get_object_class();
            let get_type_id = my_class.get_method_id("getReturnType", &[], &Kind::Object("java/lang/Class".to_string()))?;
            let get_type_output = self.call_method(&get_type_id, &[])?;
            let class_obj = get_type_output.object();
            let class_class = class_obj.get_object_class();
            let name_id = class_class.get_method_id("getName", &[], &Kind::Object("java/lang/String".to_string()))?;
            let name_output = class_obj.call_method(&name_id, &[])?;
            let name_str: strng::JavaString = TryFrom::try_from(name_output.object())?;
            let name: String = TryFrom::try_from(name_str)?;
            let my_kind: Kind = name.into();

            unsafe {
                let method_id = ((**env).from_reflected_method)(env, self.raw);
                Ok(MethodId::new(self.env, method_id, my_kind))
            }
        } else {
            Err("This is not a 'java.lang.reflect.Method' object!".into())
        }
    }

    /// Returns the fully qualified name of the class this object is an instance of as a string.
    pub fn class_name(&self) -> Result<String> {
        let my_class = self.get_object_class();
        let get_class_method_id = my_class.get_method_id("getClass", &[], &Kind::Object("java/lang/Class".to_string()))?;
        let class_obj = self.call_method(&get_class_method_id, &[])?.object();

        let class_class = class_obj.get_object_class();
        let get_name_method_id = class_class.get_method_id("getName", &[], &Kind::Object("java/lang/String".to_string()))?;
        let res = class_obj.call_method(&get_name_method_id, &[])?;
        let java_str: JavaString = TryFrom::try_from(res.object())?;
        let name = TryFrom::try_from(java_str)?;
        Ok(name)
    }
}

impl<'a> Drop for Object<'a> {
    fn drop(&mut self) {
        let env = self.env.as_ptr();
        unsafe { ((**env).delete_global_ref)(env, self.raw) };
    }
}

#[cfg(test)]
mod test {
    use object::{strng, RefType};
    use params::{Input, Kind};
    use test_jvm::att_det;

    #[test]
    fn throw() {
        assert!(att_det(|env| {
            let throwable_class = env.find_class("java/lang/Throwable")?;
            let msg = strng::JavaString::new(&env, "I'm testing throwable!")?;
            let constructor_id = throwable_class.get_method_id("<init>", &[Kind::Object("java/lang/String".to_owned())], &Kind::Void)?;
            let throwable_obj = throwable_class.new_object(&constructor_id, &[Input::StringObj(msg)])?;

            throwable_obj.throw()?;
            assert!(env.exception_check()?);
            env.exception_clear();
            Ok(())
        })
        .is_ok());
    }

    #[test]
    fn get_object_class() {
        assert!(att_det(|env| {
            let class = env.find_class("Test")?;
            let constructor_id = class.get_method_id("<init>", &[Kind::Int], &Kind::Void)?;
            let object = class.new_object(&constructor_id, &[Input::Int(5)])?;
            let obj_class = object.get_object_class();
            let other_constructor_id = obj_class.get_method_id("<init>", &[Kind::Int], &Kind::Void)?;
            let _other_obj = obj_class.new_object(&other_constructor_id, &[Input::Int(10)])?;
            Ok(())
        })
        .is_ok());
    }

    #[test]
    fn get_object_ref_type() {
        assert!(att_det(|env| {
            let class = env.find_class("Test")?;
            let constructor_id = class.get_method_id("<init>", &[Kind::Int], &Kind::Void)?;
            let object = class.new_object(&constructor_id, &[Input::Int(5)])?;
            let ref_type = object.get_object_ref_type()?;
            assert_eq!(ref_type, RefType::Global);
            Ok(())
        })
        .is_ok());
    }

    #[test]
    fn is_instance_of() {
        assert!(att_det(|env| {
            let class = env.find_class("Test")?;
            let constructor_id = class.get_method_id("<init>", &[Kind::Int], &Kind::Void)?;
            let object = class.new_object(&constructor_id, &[Input::Int(5)])?;
            assert!(object.is_instance_of(&class)?);
            Ok(())
        })
        .is_ok());
    }

    #[test]
    fn is_same_object() {
        assert!(att_det(|env| {
            let class = env.find_class("Test")?;
            let constructor_id = class.get_method_id("<init>", &[Kind::Int], &Kind::Void)?;
            let object = class.new_object(&constructor_id, &[Input::Int(5)])?;
            let new_object = env.new_global_ref(&object)?;
            assert!(object.is_same_object(&new_object)?);
            Ok(())
        })
        .is_ok());
    }

    #[test]
    fn public_methods() {
        assert!(att_det(|env| {
            let class = env.find_class("Test")?;
            let constructor_id = class.get_method_id("<init>", &[Kind::Int], &Kind::Void)?;
            let object = class.new_object(&constructor_id, &[Input::Int(5)])?;
            let inc_method_id = class.get_method_id("incrementCurrent", &[], &Kind::Void)?;
            object.call_method(&inc_method_id, &[])?;
            let get_curr_method_id = class.get_method_id("getCurrent", &[], &Kind::Int)?;
            let value = object.call_method(&get_curr_method_id, &[])?;
            assert_eq!(6, value.int());
            Ok(())
        })
        .is_ok());
    }

    #[test]
    fn public_field() {
        assert!(att_det(|env| {
            let class = env.find_class("Test")?;
            let constructor_id = class.get_method_id("<init>", &[Kind::Int], &Kind::Void)?;
            let object = class.new_object(&constructor_id, &[Input::Int(5)])?;
            let current_field_id = class.get_field_id("current", &Kind::Int)?;
            object.set_field(&current_field_id, &Input::Int(10))?;
            let current = object.get_field(&current_field_id)?;
            assert_eq!(10, current.int());
            Ok(())
        })
        .is_ok());
    }

    #[test]
    fn nonvirtual_methods() {
        assert!(att_det(|env| {
            let test_class = env.find_class("Test")?;
            let test_ext_class = env.find_class("TestExt")?;

            let test_constructor_id = test_class.get_method_id("<init>", &[Kind::Int], &Kind::Void)?;
            let test_object = test_class.new_object(&test_constructor_id, &[Input::Int(5)])?;
            let inc_method_id = test_class.get_method_id("incrementCurrent", &[], &Kind::Void)?;
            test_object.call_method(&inc_method_id, &[])?;
            let get_curr_method_id = test_class.get_method_id("getCurrent", &[], &Kind::Int)?;
            let value = test_object.call_method(&get_curr_method_id, &[])?;
            assert_eq!(6, value.int());

            // Here incrementCurrent increments by two.
            let test_ext_constructor_id = test_ext_class.get_method_id("<init>", &[Kind::Int], &Kind::Void)?;
            let test_ext_object = test_ext_class.new_object(&test_ext_constructor_id, &[Input::Int(5)])?;
            let inc_method_id_sub = test_ext_class.get_method_id("incrementCurrent", &[], &Kind::Void)?;
            test_ext_object.call_method(&inc_method_id_sub, &[])?;
            let get_curr_method_id_sub = test_ext_class.get_method_id("getCurrent", &[], &Kind::Int)?;
            let value = test_ext_object.call_method(&get_curr_method_id_sub, &[])?;
            assert_eq!(7, value.int());

            // Now call the method in the superclass, via the test_ext_obj, only incrementing by 1.
            test_ext_object.call_nonvirtual_method(&test_class, &inc_method_id, &[])?;
            let get_curr_method_id_sub = test_ext_class.get_method_id("getCurrent", &[], &Kind::Int)?;
            let value = test_ext_object.call_method(&get_curr_method_id_sub, &[])?;
            assert_eq!(8, value.int());

            Ok(())
        })
        .is_ok());
    }
}
