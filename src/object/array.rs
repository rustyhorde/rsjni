// Copyright (c) 2017 rsjni developers
//
// Licensed under the Apache License, Version 2.0
// <LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0> or the MIT
// license <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. All files in the project carrying such notice may not be copied,
// modified, or distributed except according to those terms.

//! Array wrappers for working with Java arrays as input/output types.
use class::Class;
use error::{Error, Result};
use ffi::constants::{JavaBoolean, JavaByte, JavaChar, JavaDouble, JavaFloat, JavaInteger, JavaLong, JavaShort};
use jni::Env;
use object::{self, strng};
use params::Kind;
use std::convert::TryFrom;
use std::ptr;
use utils::PrivateTryFromUsize;

macro_rules! array_wrapper {
    ($struct:ident, $ffi:ty, $rtype:ty, $new:ident, $get:ident, $get_region:ident, $set_region:ident, $release:ident) => {
        /// Convience wrapper around a regular Java object for a Java primitive array.
        #[derive(Clone, Getters)]
        pub struct $struct<'a> {
            /// The Java object.
            #[doc(hidden)]
            #[get = "pub"]
            inner: object::Object<'a>,
            /// A pointer to the array in the JVM.  We hold this until Drop so it isn't GCd.
            #[doc(hidden)]
            ptr: *mut $ffi,
            /// The length of the array in the JVM.
            len: u32,
        }

        impl<'a> $struct<'a> {
            /// Create a new array in the JVM with the give size.
            pub fn new(env: &'a Env, size: u32) -> Result<Self> {
                let env_ptr = env.as_ptr();
                unsafe {
                    let arr_ptr = ((**env_ptr).$new)(env_ptr, TryFrom::try_from(size)?);
                    let len = ((**env_ptr).get_array_length)(env_ptr, arr_ptr);
                    let arr_elem_ptr = ((**env_ptr).$get)(env_ptr, arr_ptr, ptr::null_mut());
                    Ok(Self {
                        inner: object::Object::new(env, arr_ptr),
                        ptr: arr_elem_ptr,
                        len: TryFrom::try_from(len)?,
                    })
                }
            }

            /// Copy a slice of the JVM array into a buffer.
            pub fn as_slice(&self, start: u32, len: u32, buf: &mut [$ffi]) -> Result<()> {
                let env = self.inner.env.as_ptr();

                unsafe {
                    let actual_len = ((**env).get_array_length)(env, *self.inner.raw());

                    if start + len > TryFrom::try_from(actual_len)? {
                        Err("Invalid bounds".into())
                    } else {
                        ((**env).$get_region)(env, *self.inner.raw(), TryFrom::try_from(start)?, TryFrom::try_from(len)?, buf.as_mut_ptr());
                        Ok(())
                    }
                }
            }

            /// Set a slice of the underlying JVM array.
            #[cfg(target_pointer_width = "64")]
            pub fn set_slice(&mut self, start: u32, arr: &[$rtype]) -> Result<()> {
                let env = self.inner.env.as_ptr();
                let len = arr.len();

                // Convert the list of arguments into an array of JavaValues.
                let mut java_args = Vec::with_capacity(arr.len());
                for arg in arr {
                    let java_value = *arg as $ffi;
                    java_args.push(java_value);
                }

                unsafe {
                    let actual_len = ((**env).get_array_length)(env, *self.inner.raw());
                    let given_len: u32 = u32::private_try_from(len)?;
                    if start + given_len > u32::try_from(actual_len)? {
                        Err("Invalid bounds".into())
                    } else {
                        ((**env).$set_region)(env, *self.inner.raw(), i32::try_from(start)?, i32::private_try_from(len)?, java_args.as_ptr());
                        Ok(())
                    }
                }
            }

            /// Set a slice of the underlying JVM array.
            #[cfg(target_pointer_width = "32")]
            pub fn set_slice(&mut self, start: u32, arr: &[$rtype]) -> Result<()> {
                let env = self.inner.env.as_ptr();
                let len = arr.len();

                // Convert the list of arguments into an array of JavaValues.
                let mut java_args = Vec::with_capacity(arr.len());
                for arg in arr {
                    let java_value = *arg as $ffi;
                    java_args.push(java_value);
                }

                unsafe {
                    let actual_len = ((**env).get_array_length)(env, *self.inner.raw());
                    let given_len: u32 = len as u32;
                    if start + given_len > TryFrom::try_from(actual_len)? {
                        Err("Invalid bounds".into())
                    } else {
                        ((**env).$set_region)(env, *self.inner.raw(), TryFrom::try_from(start)?, i32::private_try_from(len)?, java_args.as_ptr());
                        Ok(())
                    }
                }
            }

            /// Release the array back to the JVM, copying change contents in the process.
            pub fn release(&self) {
                let env = self.inner.env.as_ptr();
                unsafe { ((**env).$release)(env, self.inner.raw, self.ptr, 0) }
            }
        }

        impl<'a> TryFrom<object::Object<'a>> for $struct<'a> {
            type Error = Error;

            fn try_from(obj: object::Object<'a>) -> Result<Self> {
                let env = obj.env.as_ptr();

                unsafe {
                    let len = ((**env).get_array_length)(env, *obj.raw());
                    let arr_ptr = ((**env).$get)(env, *obj.raw(), ptr::null_mut());

                    Ok(Self {
                        inner: obj,
                        ptr: arr_ptr,
                        len: TryFrom::try_from(len)?,
                    })
                }
            }
        }

        impl<'a> Drop for $struct<'a> {
            fn drop(&mut self) {
                let env = self.inner.env.as_ptr();

                unsafe { ((**env).$release)(env, self.inner.raw, self.ptr, 0) };
            }
        }
    };
}

array_wrapper!(
    Boolean,
    JavaBoolean,
    bool,
    new_boolean_array,
    get_boolean_array_elements,
    get_boolean_array_region,
    set_boolean_array_region,
    release_boolean_array_elements
);
array_wrapper!(
    Byte,
    JavaByte,
    i8,
    new_byte_array,
    get_byte_array_elements,
    get_byte_array_region,
    set_byte_array_region,
    release_byte_array_elements
);
array_wrapper!(
    Char,
    JavaChar,
    char,
    new_char_array,
    get_char_array_elements,
    get_char_array_region,
    set_char_array_region,
    release_char_array_elements
);
array_wrapper!(
    Short,
    JavaShort,
    i16,
    new_short_array,
    get_short_array_elements,
    get_short_array_region,
    set_short_array_region,
    release_short_array_elements
);
array_wrapper!(
    Int,
    JavaInteger,
    i32,
    new_int_array,
    get_int_array_elements,
    get_int_array_region,
    set_int_array_region,
    release_int_array_elements
);
array_wrapper!(
    Long,
    JavaLong,
    i64,
    new_long_array,
    get_long_array_elements,
    get_long_array_region,
    set_long_array_region,
    release_long_array_elements
);
array_wrapper!(
    Float,
    JavaFloat,
    f32,
    new_float_array,
    get_float_array_elements,
    get_float_array_region,
    set_float_array_region,
    release_float_array_elements
);
array_wrapper!(
    Double,
    JavaDouble,
    f64,
    new_double_array,
    get_double_array_elements,
    get_double_array_region,
    set_double_array_region,
    release_double_array_elements
);

/// Convience wrapper around a regular Java object for a Java primitive array.
#[derive(Clone, Getters)]
pub struct Object<'a> {
    /// The Env that created this object.
    env: &'a Env,
    /// The Java object.
    #[doc(hidden)]
    #[get = "pub"]
    inner: object::Object<'a>,
    /// The class prototype that is stored in this array
    #[get = "pub"]
    class: Class<'a>,
    /// The length of the array in the JVM.
    len: u32,
}

impl<'a> Object<'a> {
    /// Create a new array that will hold `Class` type objects in the JVM with the give size.  The init object will be the initial value of the objects created
    /// in the array.
    pub fn new(env: &'a Env, class: Class<'a>, init: &object::Object<'a>, size: u32) -> Result<Self> {
        let env_ptr = env.as_ptr();
        unsafe {
            let arr_ptr = ((**env_ptr).new_object_array)(env_ptr, TryFrom::try_from(size)?, *class.raw(), *init.raw());
            let len = ((**env_ptr).get_array_length)(env_ptr, arr_ptr);
            Ok(Self {
                env,
                inner: object::Object::new(env, arr_ptr),
                class,
                len: TryFrom::try_from(len)?,
            })
        }
    }

    /// Get the class name of this object array, i.e. "[Ljava.lang.String;"
    pub fn class_name(&self) -> Result<String> {
        let my_class = self.inner.get_object_class();
        let get_class_method_id = my_class.get_method_id("getClass", &[], &Kind::Object("java/lang/Class".to_string()))?;
        let class_obj = self.inner.call_method(&get_class_method_id, &[])?.object();

        let class_class = class_obj.get_object_class();
        let get_name_method_id = class_class.get_method_id("getName", &[], &Kind::Object("java/lang/String".to_string()))?;
        let res = class_obj.call_method(&get_name_method_id, &[])?;
        let java_str: strng::JavaString = TryFrom::try_from(res.object())?;
        let name = TryFrom::try_from(java_str)?;
        Ok(name)
    }

    /// Get an element out of the `Object` array.
    pub fn get_element_at(&self, index: u32) -> Result<object::Object> {
        let env_ptr = self.env.as_ptr();

        unsafe {
            let obj = ((**env_ptr).get_object_array_element)(env_ptr, *self.inner.raw(), TryFrom::try_from(index)?);
            Ok(object::Object::new(self.env, obj))
        }
    }

    /// Set an element in the `Object` array.
    pub fn set_element_at(&self, val: &object::Object<'a>, index: u32) -> Result<()> {
        let env_ptr = self.env.as_ptr();

        unsafe {
            ((**env_ptr).set_object_array_element)(env_ptr, *self.inner.raw(), TryFrom::try_from(index)?, *val.raw());
            Ok(())
        }
    }
}

#[cfg(test)]
mod int {
    mod test {
        use object::array;
        use params::{Input, Kind};
        use std::convert::TryFrom;
        use test_jvm::att_det;

        #[test]
        fn set_slice() {
            assert!(att_det(|env| {
                // Load the class named 'Test' from the classpath.
                let class = env.find_class("Test")?;

                // Call 'Test(5)' constructor
                let constructor_id = class.get_method_id("<init>", &[Kind::Int], &Kind::Void)?;
                let object = class.new_object(&constructor_id, &[Input::Int(5)])?;

                // Work with an array input.
                let mut int_arr = array::Int::new(&env, 5)?;
                int_arr.set_slice(0, &[1; 5])?;

                // Check a slice
                let mut buf = [0; 2];
                int_arr.as_slice(0, 2, &mut buf)?;
                for (actual, expected) in buf.iter().zip(vec![1, 1].iter()) {
                    assert_eq!(*actual, *expected);
                }

                let args = [Input::IntArr(int_arr)];
                let all_ones_method_id = class.get_method_id("allOnes", &[Kind::IntArr], &Kind::Boolean)?;
                let value = object.call_method(&all_ones_method_id, &args)?;
                assert!(value.boolean());
                Ok(())
            })
            .is_ok());
        }

        #[test]
        fn array_return_type() {
            assert!(att_det(|env| {
                // Load the class named 'Test' from the classpath.
                let class = env.find_class("Test")?;

                // Call 'Test(5)' constructor
                let constructor_id = class.get_method_id("<init>", &[Kind::Int], &Kind::Void)?;
                let object = class.new_object(&constructor_id, &[Input::Int(0)])?;

                //  Work with array return value.
                let int_arr_method_id = class.get_method_id("intArr", &[], &Kind::IntArr)?;
                let int_arr_ret = object.call_method(&int_arr_method_id, &[])?;
                let int_arr: array::Int = TryFrom::try_from(int_arr_ret.object())?;

                let mut full_buf = [0; 6];
                int_arr.as_slice(0, 6, &mut full_buf)?;
                for (actual, expected) in full_buf.iter().zip(vec![1, 2, 3, 4, 5, 9].iter()) {
                    assert_eq!(*actual, *expected);
                }

                // Check a slice
                let mut buf = [0; 3];
                int_arr.as_slice(1, 3, &mut buf)?;
                for (actual, expected) in buf.iter().zip(vec![2, 3, 4].iter()) {
                    assert_eq!(*actual, *expected);
                }

                drop(int_arr);
                Ok(())
            })
            .is_ok());
        }
    }
}
