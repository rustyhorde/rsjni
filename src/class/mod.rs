// Copyright (c) 2017 rsjni developers
//
// Licensed under the Apache License, Version 2.0
// <LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0> or the MIT
// license <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. All files in the project carrying such notice may not be copied,
// modified, or distributed except according to those terms.

//! Java Class Wrapper
use error::{ErrorKind, Result};
use ffi::constants::{JavaClass, JavaFieldId, JavaMethodId};
use ffi::union::JavaValue;
use jni::Env;
use object::Object;
use params::{self, Input, Kind, Output};
use std::convert::TryFrom;
use std::ffi::CString;

/// Represents a field id found using `get_field_id`.
///
/// The class has a prescribed lifetime, since it cannot outlive the `Env` that created it.
#[derive(Clone, Debug, Eq, Getters, Hash, PartialEq)]
pub struct FieldId<'a> {
    /// The `Env` used to create this `FieldId`
    #[doc(hidden)]
    env: &'a Env,
    /// The raw JavaClass pointer.
    #[doc(hidden)]
    #[get = "pub"]
    raw: JavaFieldId,
    /// The `Kind` of this field.
    #[doc(hidden)]
    #[get = "pub"]
    kind: Kind,
}

impl<'a> FieldId<'a> {
    /// Create a new `FieldId` struct for the given `Env` and raw pointer.
    #[doc(hidden)]
    pub fn new(env: &'a Env, raw: JavaFieldId, kind: Kind) -> Self {
        Self { env, raw, kind }
    }
}

/// Represents a method id found using `get_method_id`.
///
/// The class has a prescribed lifetime, since it cannot outlive the `Env` that created it.
#[derive(Clone, Debug, Eq, Getters, Hash, PartialEq)]
pub struct MethodId<'a> {
    /// The `Env` used to create this `MethodId`
    #[doc(hidden)]
    env: &'a Env,
    /// The raw JavaClass pointer.
    #[doc(hidden)]
    #[get = "pub"]
    raw: JavaMethodId,
    /// The `Kind` of this field.
    #[doc(hidden)]
    #[get = "pub"]
    kind: Kind,
}

impl<'a> MethodId<'a> {
    /// Create a new `MethodId` struct for the given `Env` and raw pointer.
    #[doc(hidden)]
    pub fn new(env: &'a Env, raw: JavaMethodId, kind: Kind) -> Self {
        Self { env, raw, kind }
    }
}

/// Represents a Java class.  Used mainly to create Java objects.
///
/// Public static method and field access occurs through the class.
///
/// The class has a prescribed lifetime, since it cannot outlive the `Env` that created it.
#[derive(Clone, Debug, Getters)]
pub struct Class<'a> {
    /// The `Env` that loaded the class.
    env: &'a Env,
    /// The raw JavaClass pointer.
    #[doc(hidden)]
    #[get = "pub"]
    raw: JavaClass,
}

impl<'a> Class<'a> {
    /// Create a new `Class` struct for the given Jvm and raw class.
    #[doc(hidden)]
    pub fn new(env: &'a Env, raw: JavaClass) -> Self {
        Self { env, raw }
    }

    /// Get the superclass of this class.
    ///
    /// If this class represents any class other than the class `Object`, then this function returns the class that represents the superclass of this class.
    ///
    /// If this class specifies the class `Object`, or this class is an interface, this function returns `None`.
    pub fn get_superclass(&self) -> Option<Class<'a>> {
        let env = self.env.as_ptr();
        let superclass = unsafe { ((**env).get_superclass)(env, self.raw) };

        if superclass.is_null() {
            None
        } else {
            Some(Class::new(self.env, superclass))
        }
    }

    /// Determines whether an object this class can be safely cast to the given class.
    pub fn is_assignable_from<'b>(&self, class: &Class<'b>) -> bool {
        let env = self.env.as_ptr();
        let is_assignable_from = unsafe { ((**env).is_assignable_from)(env, self.raw, class.raw) };

        is_assignable_from == 1
    }

    /// Returns the `java.lang.Module` object for the module that this class is a member of. If this class is not in a named module then the unnamed module of
    /// the class loader for the class is returned. If this class represents an array type then this function returns the Module object for the element type. If
    /// this class represents a primitive type or void, then the Module object for the java.base module is returned.
    #[cfg(feature = "nine")]
    pub fn get_module(&'a self) -> Result<Object<'a>> {
        let env = self.env.as_ptr();
        let module_obj = unsafe { ((**env).get_module)(env, self.raw) };

        if module_obj.is_null() {
            Err(ErrorKind::NullPointer.into())
        } else {
            Ok(Object::new(self.env, module_obj))
        }
    }

    /// Constructs an exception object from the this class, if it is assignable from 'java/lang/Throwable', with the message specified by msg and causes that exception to be thrown.
    pub fn throw(&self, msg: &str) -> Result<()> {
        let env_ptr = self.env.as_ptr();
        let throwable_class = self.env.find_class("java/lang/Throwable")?;
        let msg_str = CString::new(msg)?;

        if self.is_assignable_from(&throwable_class) {
            unsafe {
                let status = ((**env_ptr).throw_new)(env_ptr, self.raw, msg_str.as_ptr());

                if status < 0 {
                    Err(status.into())
                } else {
                    Ok(())
                }
            }
        } else {
            Err(ErrorKind::NotAssignableFromThrowable.into())
        }
    }

    /// Allocates a new Java object **without invoking any of the constructors** for the object.
    ///
    /// # Notes
    ///
    /// * The Java Language Specification, "Implementing Finalization" (JLS §12.6.1) states: "An object o is not finalizable until its constructor has invoked
    /// the constructor for Object on o and that invocation has completed successfully". Since `alloc_object` does not invoke a constructor, objects created
    /// with this function are not eligible for finalization.
    /// * The class argument must not refer to an array class.
    pub fn alloc_object(&self) -> Result<Object<'a>> {
        let env = self.env.as_ptr();

        unsafe {
            let obj_ref = ((**env).alloc_object)(env, self.raw);

            if obj_ref.is_null() {
                Err(ErrorKind::NullPointer.into())
            } else {
                Ok(Object::new(self.env, obj_ref))
            }
        }
    }

    /// Constructs a new Java object. The method ID indicates which constructor method to invoke.
    ///
    /// # Notes
    ///
    /// * The provided arguments are for the object's constructor. The correct overloaded constructor is chosen based on the types of the arguments.
    pub fn new_object(&self, method_id: &MethodId, args: &[Input]) -> Result<Object<'a>> {
        let env = self.env.as_ptr();
        let method_id_ptr = *(*method_id).raw();

        // let name = CString::new("<init>")?;
        // let signature = CString::new(params::method_sig(args, &Kind::Void)?)?;
        // let id = unsafe { ((**env).get_method_id)(env, self.raw, name.as_ptr(), signature.as_ptr()) };

        let mut java_args = Vec::with_capacity(args.len());
        for arg in args {
            let java_value: JavaValue = TryFrom::try_from(arg)?;
            java_args.push(java_value);
        }

        let obj = unsafe { ((**env).new_object_a)(env, self.raw, method_id_ptr, java_args.as_ptr()) };

        if obj.is_null() {
            Err(ErrorKind::NullPointer.into())
        } else {
            Ok(Object::new(self.env, obj))
        }
    }

    /// Get the field id based on the information given.
    fn get_field_id_base(&self, name: &str, kind: &Kind, member_type: &MemberType) -> Result<FieldId> {
        if *kind == Kind::Void {
            return Err(ErrorKind::VoidKindForField.into());
        }
        let kind_to_save = kind.clone();
        let env = self.env.as_ptr();
        let name = CString::new(name)?;
        let kind_str: String = kind.into();
        let signature = CString::new(kind_str)?;

        unsafe {
            match *member_type {
                MemberType::Static => {
                    let id = ((**env).get_static_field_id)(env, self.raw, name.as_ptr(), signature.as_ptr());
                    if id.is_null() {
                        Err(ErrorKind::NullPointer.into())
                    } else {
                        Ok(FieldId::new(self.env, id, kind_to_save))
                    }
                }
                MemberType::NonStatic => {
                    let id = ((**env).get_field_id)(env, self.raw, name.as_ptr(), signature.as_ptr());
                    if id.is_null() {
                        Err(ErrorKind::NullPointer.into())
                    } else {
                        Ok(FieldId::new(self.env, id, kind_to_save))
                    }
                }
            }
        }
    }

    /// Returns the field ID for an instance (nonstatic) field of a class. The field is specified by its name and signature.
    ///
    /// # Notes
    ///
    /// * Causes an uninitialized class to be initialized.
    /// * Cannot be used to obtain the length field of an array. Use `get_array_length` instead.
    pub fn get_field_id(&self, name: &str, kind: &Kind) -> Result<FieldId> {
        self.get_field_id_base(name, kind, &MemberType::NonStatic)
    }

    /// Returns the field ID for a static field of a class. The field is specified by its name and signature.
    ///
    /// # Notes
    ///
    /// * `get_static_field_id` causes an uninitialized class to be initialized.
    pub fn get_static_field_id(&self, name: &str, kind: &Kind) -> Result<FieldId> {
        self.get_field_id_base(name, kind, &MemberType::Static)
    }

    ///  Get the method id based on the information given.
    fn get_method_id_base(&self, name: &str, args: &[Kind], return_type: &Kind, member_type: &MemberType) -> Result<MethodId> {
        let env = self.env.as_ptr();
        let name = CString::new(name)?;
        let signature = CString::new(params::method_sig(args, return_type)?)?;
        unsafe {
            match *member_type {
                MemberType::Static => {
                    let id = ((**env).get_static_method_id)(env, self.raw, name.as_ptr(), signature.as_ptr());
                    Ok(MethodId::new(self.env, id, return_type.clone()))
                }
                MemberType::NonStatic => {
                    let id = ((**env).get_method_id)(env, self.raw, name.as_ptr(), signature.as_ptr());
                    Ok(MethodId::new(self.env, id, return_type.clone()))
                }
            }
        }
    }

    /// Returns the method ID for an instance (nonstatic) method of a class or interface. The method may be defined in one of the class's supertypes and
    /// inherited by class. The method is determined by its name and signature.
    ///
    /// # Notes
    ///
    /// * `get_method_id` causes an uninitialized class to be initialized.
    /// * To obtain the method ID of a constructor, supply <init> as the method name and void (V) as the return type.
    pub fn get_method_id(&self, name: &str, args: &[Kind], return_type: &Kind) -> Result<MethodId> {
        self.get_method_id_base(name, args, return_type, &MemberType::NonStatic)
    }

    /// Returns the method ID for a static method of a class. The method is specified by its name and signature.
    ///
    /// # Notes
    ///
    /// * `get_static_method_id` causes an uninitialized class to be initialized.
    pub fn get_static_method_id(&self, name: &str, args: &[Kind], return_type: &Kind) -> Result<MethodId> {
        self.get_method_id_base(name, args, return_type, &MemberType::Static)
    }

    /// This routine returns the value of a static field of an object. The field to access is specified by a field ID, which is obtained by calling
    /// `get_static_field_id`.
    pub fn get_static_field(&self, field_id: &FieldId) -> Result<Output> {
        let env = self.env.as_ptr();
        let field_id_ptr = *(*field_id).raw();
        let kind = (*field_id).kind();

        // Get the contents of the field
        let result = unsafe {
            match *kind {
                Kind::Boolean => Some(JavaValue {
                    boolean: ((**env).get_static_boolean_field)(env, self.raw, field_id_ptr),
                }),
                Kind::Byte => Some(JavaValue {
                    byte: ((**env).get_static_byte_field)(env, self.raw, field_id_ptr),
                }),
                Kind::Char => Some(JavaValue {
                    character: ((**env).get_static_char_field)(env, self.raw, field_id_ptr),
                }),
                Kind::Short => Some(JavaValue {
                    short: ((**env).get_static_short_field)(env, self.raw, field_id_ptr),
                }),
                Kind::Int => Some(JavaValue {
                    integer: ((**env).get_static_int_field)(env, self.raw, field_id_ptr),
                }),
                Kind::Long => Some(JavaValue {
                    long: ((**env).get_static_long_field)(env, self.raw, field_id_ptr),
                }),
                Kind::Float => Some(JavaValue {
                    float: ((**env).get_static_float_field)(env, self.raw, field_id_ptr),
                }),
                Kind::Double => Some(JavaValue {
                    double: ((**env).get_static_double_field)(env, self.raw, field_id_ptr),
                }),
                Kind::Object(_) | Kind::BooleanArr | Kind::ByteArr | Kind::CharArr | Kind::ShortArr | Kind::IntArr | Kind::LongArr | Kind::FloatArr | Kind::DoubleArr | Kind::ObjectArr(_) => {
                    Some(JavaValue {
                        object: ((**env).get_static_object_field)(env, self.raw, field_id_ptr),
                    })
                }
                Kind::Void => return Err(ErrorKind::NotImplemented.into()),
            }
        };

        // Convert the result into a value
        if self.env.exception_check()? {
            Err(ErrorKind::NotImplemented.into())
        } else {
            Ok(Output::try_from(result, kind, self.env)?)
        }
    }

    /// This routine sets the value of a static field of an object. The field to access is specified by a field ID, which is obtained by calling
    /// `get_static_field_id`.
    pub fn set_static_field(&self, field_id: &FieldId, value: &Input) -> Result<()> {
        let env = self.env.as_ptr();
        let field_id_ptr = *(*field_id).raw();

        // Convert the value into a useable form
        let java_value: JavaValue = TryFrom::try_from(value)?;

        // Set the contents of the field
        unsafe {
            match *value {
                Input::Boolean(_) => ((**env).set_static_boolean_field)(env, self.raw, field_id_ptr, java_value.boolean),
                Input::Byte(_) => ((**env).set_static_byte_field)(env, self.raw, field_id_ptr, java_value.byte),
                Input::Char(_) => ((**env).set_static_char_field)(env, self.raw, field_id_ptr, java_value.character),
                Input::Short(_) => ((**env).set_static_short_field)(env, self.raw, field_id_ptr, java_value.short),
                Input::Int(_) => ((**env).set_static_int_field)(env, self.raw, field_id_ptr, java_value.integer),
                Input::Long(_) => ((**env).set_static_long_field)(env, self.raw, field_id_ptr, java_value.long),
                Input::Float(_) => ((**env).set_static_float_field)(env, self.raw, field_id_ptr, java_value.float),
                Input::Double(_) => ((**env).set_static_double_field)(env, self.raw, field_id_ptr, java_value.double),
                Input::Object(_)
                | Input::BooleanArr(_)
                | Input::ByteArr(_)
                | Input::CharArr(_)
                | Input::ShortArr(_)
                | Input::IntArr(_)
                | Input::LongArr(_)
                | Input::FloatArr(_)
                | Input::DoubleArr(_)
                | Input::ObjectArr(_)
                | Input::StringObj(_) => ((**env).set_static_object_field)(env, self.raw, field_id_ptr, java_value.object),
            }
        }

        // Convert the result into a value
        if self.env.exception_check()? {
            Err(ErrorKind::NotImplemented.into())
        } else {
            Ok(())
        }
    }

    /// This operation invokes a static method on a Java object, according to the specified method ID. The methodID argument must be obtained by calling
    /// `get_static_method_id`.
    ///
    /// # Notes
    ///
    /// * The method ID must be derived from class, not from one of its superclasses.
    pub fn call_static_method(&self, method_id: &MethodId, args: &[Input]) -> Result<Output> {
        let env = self.env.as_ptr();

        // Get the method ID and check it exists
        let method_id_ptr = *(*method_id).raw();
        let kind = (*method_id).kind();

        // Convert the list of arguments into an array of jvalues
        let mut java_args = Vec::with_capacity(args.len());
        for arg in args {
            let java_value: JavaValue = TryFrom::try_from(arg)?;
            java_args.push(java_value);
        }

        // Call the method
        let result = unsafe {
            match *kind {
                Kind::Boolean => Some(JavaValue {
                    boolean: ((**env).call_static_boolean_method_a)(env, self.raw, method_id_ptr, java_args.as_ptr()),
                }),
                Kind::Byte => Some(JavaValue {
                    byte: ((**env).call_static_byte_method_a)(env, self.raw, method_id_ptr, java_args.as_ptr()),
                }),
                Kind::Char => Some(JavaValue {
                    character: ((**env).call_static_char_method_a)(env, self.raw, method_id_ptr, java_args.as_ptr()),
                }),
                Kind::Short => Some(JavaValue {
                    short: ((**env).call_static_short_method_a)(env, self.raw, method_id_ptr, java_args.as_ptr()),
                }),
                Kind::Int => Some(JavaValue {
                    integer: ((**env).call_static_int_method_a)(env, self.raw, method_id_ptr, java_args.as_ptr()),
                }),
                Kind::Long => Some(JavaValue {
                    long: ((**env).call_static_long_method_a)(env, self.raw, method_id_ptr, java_args.as_ptr()),
                }),
                Kind::Float => Some(JavaValue {
                    float: ((**env).call_static_float_method_a)(env, self.raw, method_id_ptr, java_args.as_ptr()),
                }),
                Kind::Double => Some(JavaValue {
                    double: ((**env).call_static_double_method_a)(env, self.raw, method_id_ptr, java_args.as_ptr()),
                }),
                Kind::Object(_) | Kind::BooleanArr | Kind::ByteArr | Kind::CharArr | Kind::ShortArr | Kind::IntArr | Kind::LongArr | Kind::FloatArr | Kind::DoubleArr | Kind::ObjectArr(_) => {
                    Some(JavaValue {
                        object: ((**env).call_object_method_a)(env, self.raw, method_id_ptr, java_args.as_ptr()),
                    })
                }
                Kind::Void => {
                    ((**env).call_void_method_a)(env, self.raw, method_id_ptr, java_args.as_ptr());
                    None
                }
            }
        };

        // Convert the result into a value
        if self.env.exception_check()? {
            Err(ErrorKind::NotImplemented.into())
        } else {
            Ok(Output::try_from(result, kind, self.env)?)
        }
    }

    /// Converts a method ID derived from this class to a `java.lang.reflect.Method` or `java.lang.reflect.Constructor` object. `is_static` must be set to true
    /// if the method ID refers to a static field.  `is_constructor` must be set to true if you are converting a constructor.
    ///
    /// # Notes
    ///
    /// * Throws OutOfMemoryError if it fails.
    pub fn to_reflected_method(&self, method_id: &MethodId, is_constructor: bool, is_static: bool) -> Result<Output> {
        let env = self.env.as_ptr();
        let method_id_ptr = *(*method_id).raw();
        let java_bool = if is_static { 1 } else { 0 };

        unsafe {
            let method_ptr = ((**env).to_reflected_method)(env, self.raw, method_id_ptr, java_bool);

            if method_ptr.is_null() {
                Err(ErrorKind::NullPointer.into())
            } else {
                let method_jv = JavaValue { object: method_ptr };
                let kind = if is_constructor {
                    Kind::Object("java/lang/reflect/Constructor".to_string())
                } else {
                    Kind::Object("java/lang/reflect/Method".to_string())
                };
                Ok(Output::try_from(Some(method_jv), &kind, self.env)?)
            }
        }
    }

    /// Converts a field ID derived from this class to a `java.lang.reflect.Field` object. `is_static` must be set to true if the field ID refers to a static
    /// field.
    ///
    /// # Notes
    ///
    /// * Throws OutOfMemoryError if it fails.
    pub fn to_reflected_field(&self, field_id: &FieldId, is_static: bool) -> Result<Output> {
        let env = self.env.as_ptr();
        let field_id_ptr = *(*field_id).raw();
        let java_bool = if is_static { 1 } else { 0 };

        unsafe {
            let field_ptr = ((**env).to_reflected_field)(env, self.raw, field_id_ptr, java_bool);

            if field_ptr.is_null() {
                Err(ErrorKind::NullPointer.into())
            } else {
                let field_jv = JavaValue { object: field_ptr };
                Ok(Output::try_from(Some(field_jv), &Kind::Object("java/lang/reflect/Field".to_string()), self.env)?)
            }
        }
    }
}

/// Member Type
enum MemberType {
    /// Class Member
    Static,
    /// Instance Member
    NonStatic,
}

#[cfg(test)]
mod test {
    use object::strng;
    use params::{Input, Kind};
    use std::convert::TryFrom;
    use test_jvm::att_det;

    #[test]
    fn get_superclass() {
        assert!(att_det(|env| {
            let class = env.find_class("Test")?;
            assert!(class.get_superclass().is_some());
            Ok(())
        })
        .is_ok());
    }

    #[test]
    fn get_superclass_object() {
        assert!(att_det(|env| {
            let class = env.find_class("java/lang/Object")?;
            assert!(class.get_superclass().is_none());
            Ok(())
        })
        .is_ok());
    }

    #[test]
    fn is_assignable_from() {
        assert!(att_det(|env| {
            let test_class = env.find_class("Test")?;
            let obj_class = env.find_class("java/lang/Object")?;
            assert!(test_class.is_assignable_from(&obj_class));
            Ok(())
        })
        .is_ok());
    }

    #[cfg(feature = "nine")]
    #[test]
    fn get_module() {
        assert!(att_det(|env| {
            let test_class = env.find_class("Test")?;
            let mod_obj = test_class.get_module()?;
            assert_eq!(mod_obj.class_name()?, "java.lang.Module");
            Ok(())
        })
        .is_ok());
    }

    #[test]
    fn throw() {
        assert!(att_det(|env| {
            let throwable_class = env.find_class("TestException")?;
            throwable_class.throw("Ha Ha Ha, I threw up!!")?;
            assert!(env.exception_check()?);
            env.exception_clear();
            Ok(())
        })
        .is_ok());
    }

    #[test]
    fn alloc_object() {
        assert!(att_det(|env| {
            let class = env.find_class("Test")?;
            let _uninit_obj = class.alloc_object()?;
            Ok(())
        })
        .is_ok());
    }

    #[test]
    fn new_object() {
        assert!(att_det(|env| {
            let class = env.find_class("Test")?;
            let constructor_id = class.get_method_id("<init>", &[Kind::Int], &Kind::Void)?;
            class.new_object(&constructor_id, &[Input::Int(5)])?;
            Ok(())
        })
        .is_ok());
    }

    #[test]
    fn public_static_method() {
        assert!(att_det(|env| {
            let class = env.find_class("Test")?;
            let args = [Input::Int(1), Input::Int(8)];
            let add_method_id = class.get_static_method_id("add", &[Kind::Int, Kind::Int], &Kind::Int)?;
            let value = class.call_static_method(&add_method_id, &args)?;
            assert_eq!(9, value.int());
            Ok(())
        })
        .is_ok());
    }

    #[test]
    fn public_static_field() {
        assert!(att_det(|env| {
            let class = env.find_class("Test")?;

            // Get the value of the 'public static String message' field.
            let message_field_id = class.get_static_field_id("message", &Kind::Object("java/lang/String".to_string()))?;
            let current = class.get_static_field(&message_field_id)?;
            let java_str: strng::JavaString = TryFrom::try_from(current.object())?;
            let current_str: String = TryFrom::try_from(java_str)?;
            assert_eq!(current_str, "Hello!");

            // Set the value of the 'public static String message' field to "Hi!"
            let str_obj = strng::JavaString::new(&env, "Hi!")?;

            class.set_static_field(&message_field_id, &Input::StringObj(str_obj))?;

            // Get the value again to see that the static field is changed.
            let current = class.get_static_field(&message_field_id)?;
            let java_str: strng::JavaString = TryFrom::try_from(current.object())?;
            let current_str: String = TryFrom::try_from(java_str)?;
            assert_eq!(current_str, "Hi!");
            Ok(())
        })
        .is_ok());
    }

    #[test]
    fn to_reflected_method() {
        assert!(att_det(|env| {
            let class = env.find_class("Test")?;
            let ic_method_id = class.get_method_id("incrementCurrent", &[], &Kind::Void)?;
            let output = class.to_reflected_method(&ic_method_id, false, false)?;
            let method_obj = output.object();
            let method_class = method_obj.get_object_class();
            let to_string_id = method_class.get_method_id("toGenericString", &[], &Kind::Object("java/lang/String".to_string()))?;
            let to_str_output = method_obj.call_method(&to_string_id, &[])?;
            let java_str: strng::JavaString = TryFrom::try_from(to_str_output.object())?;
            let current_str: String = TryFrom::try_from(java_str)?;
            assert_eq!(current_str, "public void Test.incrementCurrent()");

            let new_method_id = method_obj.from_reflected_method()?;
            assert_eq!(new_method_id, ic_method_id);
            Ok(())
        })
        .is_ok());
    }

    #[test]
    fn to_reflected_field() {
        assert!(att_det(|env| {
            let class = env.find_class("Test")?;
            let current_field_id = class.get_field_id("current", &Kind::Int)?;
            let output = class.to_reflected_field(&current_field_id, false)?;
            let field_obj = output.object();
            let field_class = field_obj.get_object_class();
            let to_string_id = field_class.get_method_id("toGenericString", &[], &Kind::Object("java/lang/String".to_string()))?;
            let to_str_output = field_obj.call_method(&to_string_id, &[])?;
            let java_str: strng::JavaString = TryFrom::try_from(to_str_output.object())?;
            let current_str: String = TryFrom::try_from(java_str)?;
            assert_eq!(current_str, "public int Test.current");

            let new_field_id = field_obj.from_reflected_field()?;
            assert_eq!(new_field_id, current_field_id);
            Ok(())
        })
        .is_ok());
    }

    #[test]
    fn to_reflected_field_static() {
        assert!(att_det(|env| {
            let class = env.find_class("Test")?;
            let message_field_id = class.get_static_field_id("message", &Kind::Object("java/lang/String".to_string()))?;
            let output = class.to_reflected_field(&message_field_id, true)?;
            let field_obj = output.object();
            let field_class = field_obj.get_object_class();
            let to_string_id = field_class.get_method_id("toGenericString", &[], &Kind::Object("java/lang/String".to_string()))?;
            let to_str_output = field_obj.call_method(&to_string_id, &[])?;
            let java_str: strng::JavaString = TryFrom::try_from(to_str_output.object())?;
            let current_str: String = TryFrom::try_from(java_str)?;
            assert_eq!(current_str, "public static java.lang.String Test.message");
            Ok(())
        })
        .is_ok());
    }
}
