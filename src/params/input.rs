// Copyright (c) 2017 rsjni developers
//
// Licensed under the Apache License, Version 2.0
// <LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0> or the MIT
// license <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. All files in the project carrying such notice may not be copied,
// modified, or distributed except according to those terms.

//! Java Input Parameter Types
use error::{Error, Result};
use ffi::constants::{JavaBoolean, JavaByte, JavaChar, JavaDouble, JavaFloat, JavaInteger, JavaLong, JavaShort};
use ffi::union::JavaValue;
use object::strng::JavaString;
use object::{array, Object};
use std::convert::TryFrom;

/// Extracts the inner value from an `Input` type.
macro_rules! extract {
	($name:ident, $enum_name:ident, $kind:ty) => {
        /// Extract the `$kind` value from this `Input`
		pub fn $name(self) -> $kind {
			if let Input::$enum_name(value) = self {
				value
			} else {
				panic!("Cannot convert value");
			}
		}
	};
}

/// A input parameter for a method call. Note that void is not a possible input parameter type.
///
/// The parameter has a prescribed lifetime, since it cannot outlive the JVM that created it.
#[derive(Clone)]
pub enum Input<'a> {
    /// A boolean parameter, i.e boolean
    Boolean(bool),
    /// A boolean array parameter, i.e. boolean[] boolean_arr
    BooleanArr(array::Boolean<'a>),
    /// A byte parameter, i.e. byte
    Byte(i8),
    /// A byte array parameter, i.e. byte[] byte_arr
    ByteArr(array::Byte<'a>),
    /// A char parameter.
    Char(char),
    /// A char array parameter.
    CharArr(array::Char<'a>),
    /// A short parameter.
    Short(i16),
    /// A short array parameter.
    ShortArr(array::Short<'a>),
    /// An integer parameter.
    Int(i32),
    /// An integer array parameter.
    IntArr(array::Int<'a>),
    /// A long parameter.
    Long(i64),
    /// A long array parameter.
    LongArr(array::Long<'a>),
    /// A float parameter.
    Float(f32),
    /// A float array parameter.
    FloatArr(array::Float<'a>),
    /// A double parameter.
    Double(f64),
    /// A double array parameter.
    DoubleArr(array::Double<'a>),
    /// An object parameter.
    Object(Object<'a>),
    /// An object array parameter
    ObjectArr(array::Object<'a>),
    /// A String object.
    StringObj(JavaString<'a>),
}

impl<'a> Input<'a> {
    extract!(boolean, Boolean, bool);
    extract!(boolean_arr, BooleanArr, array::Boolean<'a>);
    extract!(byte, Byte, i8);
    extract!(byte_arr, ByteArr, array::Byte<'a>);
    extract!(character, Char, char);
    extract!(character_arr, CharArr, array::Char<'a>);
    extract!(short, Short, i16);
    extract!(short_arr, ShortArr, array::Short<'a>);
    extract!(int, Int, i32);
    extract!(int_arr, IntArr, array::Int<'a>);
    extract!(long, Long, i64);
    extract!(long_arr, LongArr, array::Long<'a>);
    extract!(float, Float, f32);
    extract!(float_arr, FloatArr, array::Float<'a>);
    extract!(double, Double, f64);
    extract!(double_arr, DoubleArr, array::Double<'a>);
    extract!(object, Object, Object<'a>);
    extract!(object_arr, ObjectArr, array::Object<'a>);
    extract!(strobj, StringObj, JavaString<'a>);
}

impl<'a> TryFrom<&'a Input<'a>> for JavaValue {
    type Error = Error;

    fn try_from(value: &Input) -> Result<Self> {
        let res = match *value {
            Input::Boolean(v) => Self { boolean: v as JavaBoolean },
            Input::Byte(v) => Self { byte: v as JavaByte },
            Input::Char(v) => Self { character: v as JavaChar },
            Input::Short(v) => Self { short: v as JavaShort },
            Input::Int(v) => Self { integer: v as JavaInteger },
            Input::Long(v) => Self { long: v as JavaLong },
            Input::Float(v) => Self { float: v as JavaFloat },
            Input::Double(v) => Self { double: v as JavaDouble },
            Input::Object(ref v) => Self { object: *v.raw() },
            Input::BooleanArr(ref arr) => Self { object: *arr.inner().raw() },
            Input::ByteArr(ref arr) => Self { object: *arr.inner().raw() },
            Input::CharArr(ref arr) => Self { object: *arr.inner().raw() },
            Input::ShortArr(ref arr) => Self { object: *arr.inner().raw() },
            Input::IntArr(ref arr) => Self { object: *arr.inner().raw() },
            Input::LongArr(ref arr) => Self { object: *arr.inner().raw() },
            Input::FloatArr(ref arr) => Self { object: *arr.inner().raw() },
            Input::DoubleArr(ref arr) => Self { object: *arr.inner().raw() },
            Input::ObjectArr(ref arr) => Self { object: *arr.inner().raw() },
            Input::StringObj(ref s) => Self { object: *s.inner().raw() },
        };

        Ok(res)
    }
}

impl<'a> TryFrom<&'a Input<'a>> for String {
    type Error = Error;

    fn try_from(input: &Input) -> Result<Self> {
        Ok(match *input {
            Input::Boolean(_) => "Z".to_string(),
            Input::BooleanArr(_) => "[Z".to_string(),
            Input::Byte(_) => "B".to_string(),
            Input::ByteArr(_) => "[B".to_string(),
            Input::Char(_) => "C".to_string(),
            Input::CharArr(_) => "[C".to_string(),
            Input::Short(_) => "S".to_string(),
            Input::ShortArr(_) => "[S".to_string(),
            Input::Int(_) => "I".to_string(),
            Input::IntArr(_) => "[I".to_string(),
            Input::Long(_) => "J".to_string(),
            Input::LongArr(_) => "[J".to_string(),
            Input::Float(_) => "F".to_string(),
            Input::FloatArr(_) => "[F".to_string(),
            Input::Double(_) => "D".to_string(),
            Input::DoubleArr(_) => "[D".to_string(),
            Input::Object(ref obj) => {
                let mut base = Self::from("L");
                let class_name = obj.class_name()?;
                let signature = class_name.replace('.', "/");
                base.push_str(&signature);
                base.push(';');
                base
            }
            Input::ObjectArr(ref obj_arr) => obj_arr.class_name()?,
            Input::StringObj(ref java_str) => {
                let mut base = Self::from("L");
                let class_name = java_str.inner().class_name()?;
                let signature = class_name.replace('.', "/");
                base.push_str(&signature);
                base.push(';');
                base
            }
        })
    }
}
