// Copyright (c) 2017 rsjni developers
//
// Licensed under the Apache License, Version 2.0
// <LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0> or the MIT
// license <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. All files in the project carrying such notice may not be copied,
// modified, or distributed except according to those terms.

//! Java Output Parameters
use error::{ErrorKind, Result};
use ffi::constants::JNI_TRUE;
use ffi::union::JavaValue;
use jni::Env;
use object::Object;
use params::Kind;
use std::convert::TryFrom;

/// Extracts the inner value from an `Output` type.
macro_rules! extract {
	($name:ident, $enum_name:ident, $kind:ty) => {
        /// Extract the `$kind` value from this `Output`
		pub fn $name(self) -> $kind {
			if let Output::$enum_name(value) = self {
				value
			} else {
				panic!("Cannot convert value");
			}
		}
	};
}

/// A output parameter from a method call.  Note that void is a possible type here.
///
/// The parameter has a prescribed lifetime, since it cannot outlive the JVM that created it.
#[derive(Clone)]
pub enum Output<'a> {
    /// A boolean parameter, i.e boolean
    Boolean(bool),
    /// A boolean array parameter, i.e. boolean[] boolean_arr
    BooleanArr(Object<'a>),
    /// A byte parameter, i.e. byte
    Byte(i8),
    /// A byte array parameter, i.e. byte[] byte_arr
    ByteArr(Object<'a>),
    /// A char parameter.
    Char(char),
    /// A char array parameter.
    CharArr(Object<'a>),
    /// A short parameter.
    Short(i16),
    /// A short array parameter.
    ShortArr(Object<'a>),
    /// An integer parameter.
    Int(i32),
    /// An integer array parameter.
    IntArr(Object<'a>),
    /// A long parameter.
    Long(i64),
    /// A long array parameter.
    LongArr(Object<'a>),
    /// A float parameter.
    Float(f32),
    /// A float array parameter.
    FloatArr(Object<'a>),
    /// A double parameter.
    Double(f64),
    /// A double array parameter.
    DoubleArr(Object<'a>),
    /// An object parameter.
    Object(Object<'a>),
    /// An object array parameter
    ObjectArr(Object<'a>),
    /// Void
    Void,
}

impl<'a> Output<'a> {
    /// Converts a `JavaValue` into its equivalent `Output`.
    pub fn try_from<'b>(value: Option<JavaValue>, kind: &Kind, env: &'b Env) -> Result<Output<'b>> {
        unsafe {
            Ok(match (kind, value) {
                (&Kind::Boolean, Some(ref value)) => Output::Boolean(value.boolean == u8::try_from(JNI_TRUE)?),
                (&Kind::Byte, Some(ref value)) => Output::Byte(value.byte),
                (&Kind::Char, Some(ref value)) => {
                    let as_u32: u32 = value.character.into();
                    let as_char: char = TryFrom::try_from(as_u32)?;
                    Output::Char(as_char)
                }
                (&Kind::Short, Some(ref value)) => Output::Short(value.short),
                (&Kind::Int, Some(ref value)) => Output::Int(value.integer),
                (&Kind::Long, Some(ref value)) => Output::Long(value.long),
                (&Kind::Float, Some(ref value)) => Output::Float(value.float),
                (&Kind::Double, Some(ref value)) => Output::Double(value.double),
                (&Kind::Object(_), Some(ref value))
                | (&Kind::BooleanArr, Some(ref value))
                | (&Kind::ByteArr, Some(ref value))
                | (&Kind::CharArr, Some(ref value))
                | (&Kind::ShortArr, Some(ref value))
                | (&Kind::IntArr, Some(ref value))
                | (&Kind::LongArr, Some(ref value))
                | (&Kind::FloatArr, Some(ref value))
                | (&Kind::DoubleArr, Some(ref value))
                | (&Kind::ObjectArr(_), Some(ref value)) => Output::Object(Object::new(env, value.object)),
                (&Kind::Void, None) => Output::Void,
                (_, _) => return Err(ErrorKind::NotImplemented.into()),
            })
        }
    }

    extract!(boolean, Boolean, bool);
    extract!(boolean_arr, BooleanArr, Object<'a>);
    extract!(byte, Byte, i8);
    extract!(byte_arr, ByteArr, Object<'a>);
    extract!(character, Char, char);
    extract!(character_arr, CharArr, Object<'a>);
    extract!(short, Short, i16);
    extract!(short_arr, ShortArr, Object<'a>);
    extract!(int, Int, i32);
    extract!(int_arr, IntArr, Object<'a>);
    extract!(long, Long, i64);
    extract!(long_arr, LongArr, Object<'a>);
    extract!(float, Float, f32);
    extract!(float_arr, FloatArr, Object<'a>);
    extract!(double, Double, f64);
    extract!(double_arr, DoubleArr, Object<'a>);
    extract!(object, Object, Object<'a>);
    extract!(object_arr, ObjectArr, Object<'a>);
}
