// Copyright (c) 2017 rsjni developers
//
// Licensed under the Apache License, Version 2.0
// <LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0> or the MIT
// license <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. All files in the project carrying such notice may not be copied,
// modified, or distributed except according to those terms.

//! Java Parameter Types
use error::Result;

mod input;
mod kind;
mod output;

pub use params::input::Input;
pub use params::kind::Kind;
pub use params::output::Output;

/// Returns the method signature as a string for a method with the given arguments and return type.
pub fn method_sig(args: &[Kind], return_type: &Kind) -> Result<String> {
    let mut sig = String::new();

    // Push the opening bracket for the arguments type list
    sig.push('(');

    // Iterate over each argument
    for arg in args {
        // Each Java type has a 1 character type associated with it, which we
        // push onto the signature to indicate another argument to the function
        let arg_sig: String = arg.into();
        sig.push_str(&arg_sig);
    }

    // Push the closing bracket to the arguments list
    sig.push(')');

    // Push the return type's signature
    let kind_sig: String = return_type.into();
    sig.push_str(&kind_sig);

    Ok(sig)
}

#[cfg(test)]
mod test {
    use error::Result;
    use params::{self, Kind};

    fn method_sig_res(args: &[Kind], return_type: &Kind, expected: &str) -> Result<()> {
        assert_eq!(params::method_sig(args, return_type)?, expected);
        Ok(())
    }

    #[test]
    fn method_sig() {
        assert!(method_sig_res(&[Kind::Int], &Kind::Void, "(I)V").is_ok());
    }
}
