// Copyright (c) 2017 rsjni developers
//
// Licensed under the Apache License, Version 2.0
// <LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0> or the MIT
// license <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. All files in the project carrying such notice may not be copied,
// modified, or distributed except according to those terms.

//! Java Types

/// Java Types
#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub enum Kind {
    /// A boolean
    Boolean,
    /// A boolean array
    BooleanArr,
    /// A byte
    Byte,
    /// A byte array
    ByteArr,
    /// A char.
    Char,
    /// A char array.
    CharArr,
    /// A short.
    Short,
    /// A short array.
    ShortArr,
    /// An integer.
    Int,
    /// An integer array.
    IntArr,
    /// A long.
    Long,
    /// A long array.
    LongArr,
    /// A float parameter.
    Float,
    /// A float array.
    FloatArr,
    /// A double parameter.
    Double,
    /// A double array.
    DoubleArr,
    /// An object parameter.
    Object(String),
    /// An object array.
    ObjectArr(String),
    /// Void
    Void,
}

impl<'a> From<&'a Kind> for String {
    fn from(kind: &Kind) -> Self {
        match *kind {
            Kind::Boolean => "Z".to_string(),
            Kind::BooleanArr => "[Z".to_string(),
            Kind::Byte => "B".to_string(),
            Kind::ByteArr => "[B".to_string(),
            Kind::Char => "C".to_string(),
            Kind::CharArr => "[C".to_string(),
            Kind::Short => "S".to_string(),
            Kind::ShortArr => "[S".to_string(),
            Kind::Int => "I".to_string(),
            Kind::IntArr => "[I".to_string(),
            Kind::Long => "J".to_string(),
            Kind::LongArr => "[J".to_string(),
            Kind::Float => "F".to_string(),
            Kind::FloatArr => "[F".to_string(),
            Kind::Double => "D".to_string(),
            Kind::DoubleArr => "[D".to_string(),
            Kind::Object(ref v) => {
                let mut base = Self::from("L");
                base.push_str(v);
                base.push(';');
                base
            }
            Kind::ObjectArr(ref v) => {
                let mut base = Self::from("[L");
                base.push_str(v);
                base.push(';');
                base
            }
            Kind::Void => "V".to_string(),
        }
    }
}

/// Does this string represent a Java object array?
fn is_object_array(to_test: &str) -> bool {
    to_test.starts_with("[L")
}

/// Grab the object name out of an object array signature.
fn object_name_from_sig(sig: &str) -> String {
    let len = sig.len();
    let mut obj_name = String::new();
    obj_name.push_str(&sig[2..(len - 1)]);
    obj_name.replace('.', "/")
}

impl From<String> for Kind {
    fn from(kind_str: String) -> Self {
        let matcher = &kind_str[..];
        match matcher {
            "boolean" => Kind::Boolean,
            "[Z" => Kind::BooleanArr,
            "byte" => Kind::Byte,
            "[B" => Kind::ByteArr,
            "char" => Kind::Char,
            "[C" => Kind::CharArr,
            "short" => Kind::Short,
            "[S" => Kind::ShortArr,
            "int" => Kind::Int,
            "[I" => Kind::IntArr,
            "long" => Kind::Long,
            "[J" => Kind::LongArr,
            "float" => Kind::Float,
            "[F" => Kind::FloatArr,
            "double" => Kind::Double,
            "[D" => Kind::DoubleArr,
            "void" => Kind::Void,
            obj => {
                if is_object_array(obj) {
                    Kind::ObjectArr(object_name_from_sig(obj))
                } else {
                    Kind::Object(obj.replace('.', "/"))
                }
            }
        }
    }
}

#[cfg(test)]
mod test {
    use super::object_name_from_sig;

    #[test]
    fn sig_to_name() {
        assert_eq!("java/lang/String", object_name_from_sig("[Ljava.lang.String;"));
    }
}
