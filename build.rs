use std::env;

fn main() {
    if let Ok(java_home) = env::var("JAVA_HOME") {
        println!("cargo:rustc-link-lib=jvm");
        #[cfg(all(feature = "eight", target_arch = "x86", target_os = "linux"))]
        println!("cargo:rustc-link-search={}/jre/lib/i386/server", java_home);
        #[cfg(all(target_arch = "x86_64", target_os = "windows", target_env = "msvc"))]
        println!("cargo:rustc-link-search={}/lib", java_home);
        #[cfg(all(feature = "eight", target_arch = "x86_64", target_os = "linux"))]
        println!("cargo:rustc-link-search={}/jre/lib/amd64/server", java_home);
        #[cfg(all(feature = "nine", target_arch = "x86_64", target_os = "linux"))]
        println!("cargo:rustc-link-search={}/lib/server", java_home);
        #[cfg(all(feature = "eight", target_arch = "x86_64", target_os = "macos"))]
        println!("cargo:rustc-link-search={}/jre/lib/server", java_home);
        #[cfg(all(feature = "ten", target_arch = "x86_64", target_os = "linux"))]
        println!("cargo:rustc-link-search={}/lib/server", java_home);
    } else {
        panic!("Unable to find JAVA_HOME to link libjvm");
    }
}
